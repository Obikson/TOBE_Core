﻿using UnityEngine;
using Tobe.Core.Mvc.Abstractions;

namespace Tobe.Core
{
    public class DelegatedCollider : AViewComponent
    {
        [SerializeField]
        private GameObject parent;

        public GameObject Parent => parent;

        public TComponent GetParent<TComponent>() where TComponent : Component
        {
            return parent.GetComponent<TComponent>();
        }
    }
}