﻿using UnityEngine;
using Random = System.Random;

namespace Tobe.Core
{
    public static class QuaternionHelpers
    {
        private static Random _random;

        static QuaternionHelpers()
        {
            _random = new Random();
        }

        
        public static Quaternion Clamp(this Quaternion source, Quaternion min, Quaternion max)
        {
            return new Quaternion(
                Mathf.Clamp(source.x, min.x, max.x),
                Mathf.Clamp(source.y, min.y, max.y),
                Mathf.Clamp(source.z, min.z, max.z),
                source.w
            );
        }

        public static Quaternion RandomRange(Vector3 minRotationRange, Vector3 maxRotationRange)
        {
            var r = VectorHelpers.RandomRange(minRotationRange, maxRotationRange);
            
            return Quaternion.Euler(r);
        }

        public static Quaternion Clamp(this Quaternion source, Vector3 minRotationRange, Vector3 maxRotationRange)
        {
            return Clamp(source, Quaternion.Euler(minRotationRange), Quaternion.Euler(maxRotationRange));
        }
    }
}