﻿public class ReusableTimer : Timer
{
    public int UsedCount { get; private set; }

    public ReusableTimer(float period, bool isTimeScaled = true) : base(period, isTimeScaled)
    {
    }

    protected override void UpdateInternal()
    {
        if (CurrentTime >= Period)
        {
            Interval.OnNext(this);
            
            Pause();
        }
    }
    
    public void Reuse()
    {
        UsedCount++;
        Reset();
        Resume();
    }
}



public class CyclicTimer : Timer
{
    public int MaxCycles { get; }
    
    public int CurrentCycle { get; private set; }
    
    public CyclicTimer(float period, int maxCycles, bool isTimeScaled = true) : base(period, isTimeScaled)
    {
        MaxCycles = maxCycles;
    }

    protected override void UpdateInternal()
    {
        if (CurrentTime >= Period)
        {
            CurrentCycle++;
            
            Interval.OnNext(this);
            
            if (CurrentCycle >= MaxCycles)
            {
                Dispose();
            }
            else
            {
                Reset();
            }
        }
    }
}