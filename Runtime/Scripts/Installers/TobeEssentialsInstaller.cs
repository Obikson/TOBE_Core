﻿using Tobe.Core;
using Tobe.Core.Mvc;
using Tobe.Core.Mvc.Implementations;
using Zenject;

public class TobeEssentialsInstaller : Installer<TobeEssentialsInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<CycleManager>().AsSingle();
        Container.BindInterfacesAndSelfTo<CoroutineProcessor>().FromNewComponentOnNewGameObject().AsSingle();
        Container.BindViewController<ISimpleView, ISimpleController>().WithControllerImplementation<SimpleController>();
    }
}