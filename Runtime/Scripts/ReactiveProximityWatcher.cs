﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using Zenject;

namespace Tobe.Core
{
    public class ReactiveProximityWatcher : MonoBehaviour, IDisposable
    {
#if UNITY_EDITOR
        [SerializeField, HorizontalGroup(300), ValueDropdown(nameof(ProjectTags))]
        private string selectedTag;

        [Button(ButtonSizes.Small, ButtonStyle.Box, Name = "+"), HorizontalGroup(20)]
        private void AddWatchTag()
        {
            if (watchedTags.Contains(selectedTag) || string.IsNullOrEmpty(selectedTag))
            {
                return;
            }

            watchedTags = watchedTags.Append(selectedTag).ToArray();
        }

        [Button(ButtonSizes.Small, ButtonStyle.Box, Name = "-"), HorizontalGroup(20)]
        private void RemoveWatchedTag()
        {
            if (!watchedTags.Contains(selectedTag) || string.IsNullOrEmpty(selectedTag))
            {
                return;
            }

            watchedTags = watchedTags.Where(x => x != selectedTag).ToArray();
        }

        private static string[] ProjectTags()
        {
            return UnityEditorInternal.InternalEditorUtility.tags;
        }
#endif


        [SerializeField, ReadOnly] private string[] watchedTags;

        [SerializeField] private string[] ignoreTags;


        public interface IProximityData
        {
            bool IsCollision { get; }

            GameObject GameObject { get; }
        }

        private class ProximityTriggerData : IProximityData
        {
            public bool IsCollision => false;
            public GameObject GameObject { get; internal set; }
            public Collider Collider { get; internal set; }
        }

        private class ProximityCollisionData : IProximityData
        {
            public bool IsCollision => true;

            public GameObject GameObject { get; internal set; }
            public Collision Collision { get; internal set; }
        }

        public IObservable<IProximityData> OnGameObjectEnter => _onGameObjectEnter;
        public IObservable<IProximityData> OnGameObjectExit => _onGameObjectExit;

        private Subject<IProximityData> _onGameObjectEnter;

        private Subject<IProximityData> _onGameObjectExit;

        private ProximityTriggerData _triggerData = new ProximityTriggerData();

        private ProximityCollisionData _collisionData = new ProximityCollisionData();

        [Inject]
        private void Ctor()
        {
            _onGameObjectEnter = new Subject<IProximityData>();

            _onGameObjectExit = new Subject<IProximityData>();
        }

        private void OnCollisionEnter(Collision other)
        {
            var collidingGo = GetObject(other.collider);

            if (!collidingGo) return;

            _collisionData.GameObject = collidingGo;
            _collisionData.Collision = other;

            _onGameObjectEnter.OnNext(_collisionData);
        }

        private void OnCollisionExit(Collision other)
        {
            var collidingGo = GetObject(other.collider);
            if (!collidingGo) return;

            _collisionData.GameObject = collidingGo;
            _collisionData.Collision = other;
            _onGameObjectExit.OnNext(_collisionData);
        }

        private GameObject GetObject(Component other)
        {
            if (watchedTags.Length > 0 && !watchedTags.Contains(other.gameObject.tag)) return null;

            if (ignoreTags != null && ignoreTags.Length > 0 && ignoreTags.Any(other.CompareTag))
            {
                return null;
            }

            var collidingGo = other.gameObject;

            var delegatedCollider = collidingGo.GetComponent<DelegatedCollider>();
            if (delegatedCollider)
            {
                collidingGo = delegatedCollider.Parent;
            }

            return collidingGo;
        }

        private void OnTriggerEnter(Collider other)
        {
            var collidingGo = GetObject(other);

            if (!collidingGo) return;

            _triggerData.GameObject = collidingGo;
            _triggerData.Collider = other;
            _onGameObjectEnter.OnNext(_triggerData);
        }

        private void OnTriggerExit(Collider other)
        {
            var collidingGo = GetObject(other);

            if (!collidingGo) return;
            _triggerData.Collider = other;

            _triggerData.GameObject = collidingGo;
            _onGameObjectExit.OnNext(_triggerData);
        }

        public void Dispose()
        {
            _onGameObjectEnter?.Dispose();
            _onGameObjectExit?.Dispose();
        }
    }
}