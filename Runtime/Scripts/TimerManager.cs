﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Zenject;

public class TimerManager : MonoBehaviour, ITimerManager
{
    private readonly List<Timer> _singleUseTimers = new List<Timer>();

    private readonly List<Timer> _cyclicTimers = new List<Timer>();

    private readonly List<Timer> _reusableTimers = new List<Timer>();

    private SingleUseTimer SingleUseTimer(float period, bool isTimeScaled)
    {
        SingleUseTimer result;

        _singleUseTimers.Add(result = new SingleUseTimer(period, isTimeScaled));

        result.Subscribe(t => { }, () => { _singleUseTimers.Remove(result); });

        return result;
    }

    private CyclicTimer CyclicUseTimer(float period, int cycles, bool isTimeScaled)
    {
        CyclicTimer result;

        _cyclicTimers.Add(result = new CyclicTimer(period, cycles, isTimeScaled));

        result.Subscribe(t => { }, () => { _cyclicTimers.Remove(result); });

        return result;
    }


    private ReusableTimer ReusableTimer(float period, bool isTimeScaled)
    {
        ReusableTimer result;

        _reusableTimers.Add(result = new ReusableTimer(period, isTimeScaled));

        result.Subscribe(t => { }, () => { _reusableTimers.Remove(result); });

        return result;
    }


    public void Dispose()
    {
        DisposeTimersContainer(_singleUseTimers);
        DisposeTimersContainer(_cyclicTimers);
        DisposeTimersContainer(_reusableTimers);

#if UNITY_EDITOR
        print("Timers disposed");
#endif
    }

    private void DisposeTimersContainer(ICollection<Timer> timers)
    {
        for (int i = 0; i < timers.Count; i++)
        {
            var singleUseTimer = timers.ElementAt(i);

            singleUseTimer.Dispose();
        }

        timers.Clear();
    }

    public SingleUseTimer CreateSingleUseScaledTimer(float period)
    {
        return SingleUseTimer(period, true);
    }

    public SingleUseTimer CreateSingleUseUnscaledTimer(float period)
    {
        return SingleUseTimer(period, false);
    }

    public CyclicTimer CreateCyclicScaledTimer(float period, int cycles)
    {
        return CyclicUseTimer(period, cycles, true);
    }

    public CyclicTimer CreatePeriodicalUnscaledTimer(float period, int cycles)
    {
        return CyclicUseTimer(period, cycles, false);
    }

    public ReusableTimer CreateReusableScaledTimer(float period)
    {
        return ReusableTimer(period, true);
    }

    public ReusableTimer CreateReusableUnscaledTimer(float period)
    {
        return ReusableTimer(period, false);
    }

    public int TimersCount => _singleUseTimers.Count + _reusableTimers.Count + _cyclicTimers.Count;

    private void Update()
    {
        UpdateTimers(_singleUseTimers);
        UpdateTimers(_cyclicTimers);
        UpdateTimers(_reusableTimers);
    }

    private static void UpdateTimers(ICollection<Timer> timers)
    {
        for (var index = 0; index < timers.Count; index++)
        {
            var singleUseTimer = timers.ElementAt(index);

            if (singleUseTimer != null && !singleUseTimer.IsDisposed)
            {
                singleUseTimer.Update();
            }
        }
    }
}