﻿using System;

namespace Tobe.Core.Abstractions
{
    public interface IObservableValue<TValue> : IDisposable
    {
        event EventHandler<ObservableValueEventHandler<TValue>> Changed;
        TValue Value { get; set; }
        
        void SetSilent(TValue value);
    }
}