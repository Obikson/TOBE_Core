﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Tobe.Core.Abstractions
{
    public abstract class ListView<TListItem> : MonoBehaviour where TListItem : Object, IDisposable
    {
        [SerializeField] protected TListItem listItem;

        [SerializeField] protected Transform container;

        public DisplayStateProperty Display { get; private set; }

        private Func<Transform, TListItem> _listItemInstantiation;

        private IInstantiator _instantiator;
        
        public Func<Transform, TListItem> ListItemInstantiation
        {
            get
            {
                return _listItemInstantiation ??
                       (_listItemInstantiation = parent => _instantiator.InstantiatePrefabForComponent<TListItem>(listItem, parent));
            }
        }

        [SerializeField]
        private DisplayState initialState = DisplayState.Block;
        
        [Inject]
        private void Ctor(IInstantiator instantiator)
        {
            _instantiator = instantiator;
            Display = new DisplayStateProperty();
            
            Display.Subscribe(x =>
            {
                gameObject.SetActive(x != DisplayState.None);
            });
            Display.Value = initialState;
        }

        private void Awake()
        {
            OnAwake();
        }

        protected virtual void OnAwake()
        {
        }

        public void BindData<TDataModel>(IEnumerable<TDataModel> models, Action<TDataModel, TListItem> onBind)
        {
            container.ClearChildren();

            foreach (var dataModel in models)
            {
                AddSingleItem(dataModel, onBind);
            }
        }

        public int Count => container.childCount;

        public void PopTop()
        {
            if (container.childCount <= 0) return;

            RemoveAt(0);
        }

        public void RemoveAt(int index)
        {
            var child = container.GetChild(index);

            Destroy(child.gameObject);
        }

        public TListItem GetItem(int index) => transform.GetChild(index).GetComponent<TListItem>();

        public TListItem First => GetItem(0);

        public TListItem Last => GetItem(transform.childCount - 1);

        public void PopBot()
        {
            if (container.childCount <= 0) return;

            RemoveAt(container.childCount - 1);
        }

        public void AddSingleItem<TDataModel>(TDataModel model, Action<TDataModel, TListItem> onBind)
        {
            var itemInstance = ListItemInstantiation(container);

            onBind(model, itemInstance);
        }

        public void Clear()
        {
            container.ClearChildren();
        }

        private void OnDestroy()
        {
            Dispose();
        }

        public virtual void Dispose()
        {
        }
    }
}