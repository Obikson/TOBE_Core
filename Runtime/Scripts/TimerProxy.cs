﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Zenject;
using UniRx;

public enum TimerType
{
    SingleUse,
    Cyclic
}

[Serializable]
public class TimerProxyUnityEvent : UnityEvent<float>
{
}

public class TimerProxy : MonoBehaviour
{
    private ITimerManager _timerManager;

    private Timer _timer;
    
    [TabGroup("General")] public float period;

    [TabGroup("General")]
    public bool isCountdown;
    
    [SerializeField] [TabGroup("General")] [ShowIf(nameof(timerType), TimerType.SingleUse)]
    private bool isReusable;

    [SerializeField] [TabGroup("General")] [ShowIf(nameof(timerType), TimerType.Cyclic)]
    private int cyclesCount;

    [TabGroup("General")] [SerializeField] private TimerType timerType;

    [TabGroup("Events")] [SerializeField] private bool trackOnTick;

    [ShowIf(nameof(trackOnTick))] [TabGroup("Events")]
    public TimerProxyUnityEvent onTick;

    [TabGroup("Events")] public UnityEvent onIntervalCompleted;

    [TabGroup("Events")] public UnityEvent onStart;

    [Inject]
    private void Ctor(ITimerManager timerManager)
    {
        _timerManager = timerManager;
    }

    private void Start()
    {
        switch (timerType)
        {
            case TimerType.SingleUse:
                if (isReusable)
                {
                    _timer = _timerManager.CreateReusableScaledTimer(period);
                }
                else
                {
                    _timer = _timerManager.CreateSingleUseScaledTimer(period);
                }

                break;
            case TimerType.Cyclic:
                _timer = _timerManager.CreateCyclicScaledTimer(period, cyclesCount);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        if (_timer != null)
        {
            _timer.IsCountdown = isCountdown;
            _timer.Subscribe(t =>  onIntervalCompleted?.Invoke());

            if (trackOnTick)
            {
                _timer.Tick.Subscribe(t => onTick?.Invoke(_timer.GetPeriodPercentage()));
            }
        }
    }

    public void StartTimer()
    {
        onStart.Invoke();

        if (_timer is ReusableTimer reusableTimer)
        {
            reusableTimer.Reuse();
        }
        else
        {
            _timer.Reset();
        
            _timer.Resume();
        }
    }

    public void PauseTimer()
    {
        _timer.Pause();
    }

    public void ResetTimer()
    {
        _timer.Reset();
    }
}