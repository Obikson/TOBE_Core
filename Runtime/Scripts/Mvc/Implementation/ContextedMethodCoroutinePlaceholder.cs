﻿using System;
using System.Collections;
using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    public class ContextedMethodCoroutinePlaceholder<TContext> : ICoroutineLogicPlaceHolder<TContext> where TContext : class
    {
        public IEnumerator Logic => null;
        public IEnumerator ProcessLogic(TContext context)
        {
            _action?.Invoke(context);

            return null;
        }

        private readonly Action<TContext> _action;


        public ContextedMethodCoroutinePlaceholder(Action<TContext> action)
        {
            _action = action;
        }

        public void Dispose()
        {
        }
    }
    
    
}