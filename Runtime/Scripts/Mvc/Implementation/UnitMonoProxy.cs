﻿using System;
using UnityEngine;
using Zenject;
using UniRx;

public interface IUnitDialogProvider
{
    DialogLine[] GetDialogLines(IUnitIdentifier unitIdentifier, Vector3 unitPosition);
}

public class NpcDialogProvider : IUnitDialogProvider
{
    private readonly DialogLine[] _lines;


    public NpcDialogProvider(DialogLine[] lines)
    {
        _lines = lines;
    }

    public DialogLine[] GetDialogLines(IUnitIdentifier unitIdentifier, Vector3 unitPosition)
    {
       
        return _lines;
    }
}

public class UnitMonoProxySpawnArgs
{
}

public class UnitMonoProxy : MonoBehaviour, IAiTarget, IDialogTarget, IPoolable<UnitMonoProxySpawnArgs, IMemoryPool>,
    IDisposable
{
    private IMemoryPool _pool;

    private SignalBus _signalBus;

    private ICommander _commander;

    public class Factory : PlaceholderFactory<UnitMonoProxySpawnArgs, UnitMonoProxy>
    {
    }
    
    public Vector3 WorldPosition => transform.position;

    public IUnitIdentifier UnitIdentifier { get; private set; }

    public IUnitDisplayDataProvider DisplayDataProvider { get; private set; }

    public IUnitDialogProvider DialogProvider { get; private set; }

    private IInstanceContext _instanceContext;
    
    [Inject]
    private void Ctor(SignalBus signalBus, 
        ISplashDamageObserver splashDamageObserver, 
        IUnitIdentifier unitIdentifier,
        IUnitDisplayDataProvider unitDisplayDataProvider, 
        IInstanceContext instanceContext,
        ICommander commander = null, 
        IUnitDialogProvider dialogProvider = null)
    {
        _signalBus = signalBus;

        _commander = commander;
        
        DisplayDataProvider = unitDisplayDataProvider;
        
        DialogProvider = dialogProvider;
        
        UnitIdentifier = unitIdentifier;

        _instanceContext = instanceContext;
        
        splashDamageObserver.SplashDamage
            .Where(x => x.DamageSource.UnitIdentifier == null ||
                        x.DamageSource.UnitIdentifier != unitIdentifier) // Dont' hit self
            .Where(x => Vector3.Distance(x.Origin, transform.position) <= x.Range) // Be in range // TODO: angle resolve
            .Subscribe(x => { TakeHit(x.DamageSource, x.Amount); });
    }

    public void TakeHit(IDamageSource damageSource, int damageAmount)
    {
        _signalBus.TryFire(new DamageSignal
        {
            DamageAmount = damageAmount,
            DamageSource = damageSource
        });
    }

    public void OnDespawned()
    {
        _pool = null;
        _instanceContext.UnregisterTarget(this);
    }

    public void OnSpawned(UnitMonoProxySpawnArgs spawnArgs, IMemoryPool pool)
    {
        _pool = pool;
        _instanceContext.RegisterTarget(this);

    }

    public void Dispose()
    {
        _pool?.Despawn(this);
    }

    
    public bool TryBeginDialog(IUnitIdentifier initiator, Vector3 initiatorWorldPosition)
    {
        if (_commander == null)
            return false;
        
        return _commander.TryInterupt(new DialogCommand
        {
            InitiatorPosition = initiatorWorldPosition,
            UnitIdentifier = initiator
        });
    }
}