﻿namespace Tobe.Core.Mvc.Implementations
{
    public enum ViewModelUpdateStrategy
    {
        OnDemand,
        OnUpdate,
        WhenDirty
    }
}