﻿using System;
using Sirenix.OdinInspector;
using Tobe.Core.Mvc.Implementations;
using UnityEngine;

[Serializable]
public class UnitViewModel : IDirtyModel
{
    [SerializeField] private int groupId;

    [SerializeField, HideIf(nameof(npcMetadata))]
    private  string npcId;

    public bool IsDirty { get; set; }

    public int GroupId
    {
        get => groupId;
        set => groupId = value;
    }


    public string NpcId => npcId;

    
    public NpcMetadata npcMetadata;
}