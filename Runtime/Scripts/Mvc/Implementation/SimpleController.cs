﻿namespace Tobe.Core.Mvc.Implementations
{
    public class SimpleController : Controller<ISimpleView, SimpleViewModel>, ISimpleController
    {
        public void SetVisible(bool value)
        {
            ViewModel.isVisible = value;
        }

        public void ToggleVisible()
        {
            ViewModel.isVisible = !ViewModel.isVisible;
        }

        public void ToggleActive()
        {
            ViewModel.isActive = !ViewModel.isActive;
            View.RenderModel(ViewModel);
        }

        public void SetActive(bool value)
        {
            ViewModel.isActive = value;
        }
    }
}