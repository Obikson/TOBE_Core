﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    public class CoroutineBuilder<TContext> : CoroutineBuilder where TContext : class
    {
        private readonly TContext _context;

        public CoroutineBuilder(ICoroutineProcessor processor, TContext context) : base(processor)
        {
            _context = context;
        }

        public CoroutineBuilder Then(Action<TContext> logic)
        {
            _coroutines.Add(new ContextedFuncCoroutinePlaceholder<TContext>(logic));

            return this;
        }

        public override CoroutineBuilder Execute()
        {
            var coroutines = _coroutines.ToArray();

            Coroutine = Processor.Process(coroutines.Cast<ICoroutineLogicPlaceHolder<TContext>>() , _context, Dispose);

            return this;
        }
    }

    public class CoroutineBuilder : IDisposable
    {
        protected readonly List<ICoroutineLogicPlaceHolder> _coroutines;

        protected ICoroutineProcessor Processor { get; }

        protected Coroutine Coroutine;

        private bool _disposed;

        public IReadOnlyCollection<ICoroutineLogicPlaceHolder> Coroutines => _coroutines;


        public CoroutineBuilder(ICoroutineProcessor processor)
        {
            _coroutines = new List<ICoroutineLogicPlaceHolder>();
            Processor = processor;
        }

        public CoroutineBuilder WaitForSeconds(float seconds)
        {
            _coroutines.Add(WaitForSecondsInternal(seconds));

            return this;
        }

        public void OnComplete(Action action)
        {
            _coroutines.Add(new CoroutineCompletion(action));

            Execute();
        }

        public CoroutineBuilder Then(ICoroutineLogicPlaceHolder logic)
        {
            _coroutines.Add(logic);

            return this;
        }

        public CoroutineBuilder Then(Action logic)
        {
            _coroutines.Add(new ActionCoroutinePlaceholder(logic));

            return this;
        }

        /// <summary>
        /// Call dispose to terminate coroutine!
        /// </summary>
        /// <returns></returns>
        public virtual CoroutineBuilder Execute()
        {
            var coroutines = _coroutines.ToArray();

            Coroutine = Processor.Process(coroutines, Dispose);

            return this;
        }

        private static ICoroutineLogicPlaceHolder WaitForSecondsInternal(float seconds)
        {
            return new FuncCoroutinePlaceholder(() => new WaitForSeconds(seconds));
        }

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;

            if (Coroutine != null)
            {
                Processor.Stop(Coroutine);
            }

            _coroutines.Clear();
        }
    }
}