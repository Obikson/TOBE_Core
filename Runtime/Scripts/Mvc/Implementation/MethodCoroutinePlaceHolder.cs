﻿using System.Collections;

namespace Tobe.Core.Mvc.Implementations
{
    public class MethodCoroutinePlaceHolder : ICoroutineLogicPlaceHolder
    {
        public IEnumerator Logic { get; private set; }

        public MethodCoroutinePlaceHolder(IEnumerator logic)
        {
            Logic = logic;
        }

        public void Dispose()
        {
            Logic = null;
        }
    }
}