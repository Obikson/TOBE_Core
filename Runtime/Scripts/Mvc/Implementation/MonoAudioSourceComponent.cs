﻿using UnityEngine;
using Tobe.Core.Mvc.Abstractions;

namespace Tobe.Core.Mvc.Implementations
{
    [RequireComponent(typeof(AudioSource))]
    public class MonoAudioSourceComponent : AViewComponent, IAudioSourceComponent
    {
        [SerializeField] private AudioSource audioSource;
        protected override void OnAwoke()
        {
            audioSource = GetComponent<AudioSource>();
        }

        public void PlayOneShot(AudioClip clip)
        {
            audioSource.PlayOneShot(clip);
        }

        public void PlayOneShot(AudioClip clip, float volume)
        {
            audioSource.PlayOneShot(clip, volume);
        }

        public void Play()
        {
            audioSource.Play();
        }

        public void Play(AudioClip clip)
        {
            audioSource.clip = clip;
            Play();
        }

        public void SetVolume(float volumeScale)
        {
            audioSource.volume = volumeScale;
        }

        public void Stop()
        {
            audioSource.Stop();
        }

        public void Pause()
        {
            audioSource.Pause();
        }

        public void UnPause()
        {
            audioSource.UnPause();
        }
    }
}