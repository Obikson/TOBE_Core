﻿using System;
using System.Collections;

namespace Tobe.Core.Mvc.Implementations
{
    public class ActionCoroutinePlaceholder : ICoroutineLogicPlaceHolder
    {
        public void Dispose()
        {
        }

        public IEnumerator Logic 
        {
            get
            {
                _action?.Invoke();
                yield return null;
            }
        }

        private readonly Action _action;
        
        public ActionCoroutinePlaceholder(Action action)
        {
            _action = action;
        }
    }
}