﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    public class CoroutineProcessor : MonoBehaviour, ICoroutineProcessor, IDisposable
    {
       

        private void OnDestroy()
        {
            Dispose();
        }

        public void Dispose()
        {
            StopAllCoroutines();
        }

        private static IEnumerator LogicProcess(ICoroutineLogicPlaceHolder logicPlaceHolder, Action onComplete)
        {
            yield return logicPlaceHolder.Logic;
            
            onComplete?.Invoke();
        }
        
        private static IEnumerator LogicProcess(IEnumerable<ICoroutineLogicPlaceHolder> logicPlaceHolders, Action onComplete)
        {
            foreach (var coroutineLogicPlaceHolder in logicPlaceHolders)
            {
                yield return coroutineLogicPlaceHolder.Logic;
            }
            
            onComplete?.Invoke();
        }
        
        private static IEnumerator LogicProcess<TContext>(IEnumerable<ICoroutineLogicPlaceHolder<TContext>> logicPlaceHolders, TContext context, Action onComplete) where TContext : class
        {
            foreach (var coroutineLogicPlaceHolder in logicPlaceHolders)
            {
                yield return coroutineLogicPlaceHolder.ProcessLogic(context);
            }
            
            onComplete?.Invoke();
        }
        
        private static IEnumerator LogicProcess<TContext>(ICoroutineLogicPlaceHolder<TContext> logic, TContext context, Action onComplete) where TContext : class
        {
            yield return logic.ProcessLogic(context);
            
            onComplete?.Invoke();
        }

        
        public Coroutine Process(ICoroutineLogicPlaceHolder logic, Action onCompleted)
        {
            var result = StartCoroutine(LogicProcess(logic, onCompleted));

            return result;
        }

        public Coroutine Process(IEnumerable<ICoroutineLogicPlaceHolder> logic, Action onCompleted)
        {
            var result = StartCoroutine(LogicProcess(logic, onCompleted));

            return result;
        }

        public Coroutine Process<TContext>(ICoroutineLogicPlaceHolder<TContext> logic, TContext context, Action onCompleted) where TContext : class
        {
            var result = StartCoroutine(LogicProcess(logic, context, onCompleted));

            return result;
        }

        public Coroutine Process<TContext>(IEnumerable<ICoroutineLogicPlaceHolder<TContext>> logic, TContext context, Action onCompleted) where TContext : class
        {
            var result = StartCoroutine(LogicProcess(logic, context, onCompleted));

            return result;
        }

        public void Stop(Coroutine coroutine)
        {
            StopCoroutine(coroutine);
        }
        
        
    }
}