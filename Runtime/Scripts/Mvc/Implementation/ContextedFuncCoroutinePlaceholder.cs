﻿using System;
using System.Collections;
using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    public class ContextedFuncCoroutinePlaceholder<TContext> : ICoroutineLogicPlaceHolder<TContext>
        where TContext : class
    {
        public IEnumerator Logic => null;

        public IEnumerator ProcessLogic(TContext context)
        {
            _logic?.Invoke(context);
            
            yield return null;
        }

        private readonly Action<TContext> _logic;

        public ContextedFuncCoroutinePlaceholder(Action<TContext> logic)
        {
            _logic = logic;
        }

        public void Dispose()
        {
        }
    }
}