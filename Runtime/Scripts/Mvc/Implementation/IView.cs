﻿namespace Tobe.Core.Mvc.Implementations
{
    public interface IView<in TViewModel> where TViewModel : IDirtyModel
    {
        void RenderModel(TViewModel model);
    }
}