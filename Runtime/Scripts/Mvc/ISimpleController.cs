﻿namespace Tobe.Core.Mvc
{
    public interface ISimpleController : IController<ISimpleView, SimpleViewModel>
    {
        void SetVisible(bool value);
        void ToggleVisible();

        void ToggleActive();

        void SetActive(bool value);
    }
}