﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tobe.Core.Mvc.Implementations;
using UnityEngine;

namespace Tobe.Core.Mvc
{
    public interface ICoroutineProcessor
    {
        Coroutine Process(ICoroutineLogicPlaceHolder logic, Action onCompleted);
        Coroutine Process(IEnumerable<ICoroutineLogicPlaceHolder> logic, Action onCompleted);
        
        Coroutine Process<TContext>(ICoroutineLogicPlaceHolder<TContext> logic, TContext context, Action onCompleted) where TContext : class;

        Coroutine Process<TContext>(IEnumerable<ICoroutineLogicPlaceHolder<TContext>> logic, TContext context, Action onCompleted) where TContext : class;

        void Stop(Coroutine coroutine);
    }
}