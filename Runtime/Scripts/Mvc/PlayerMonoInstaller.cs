﻿using Tobe.Core;
using UnityEngine;
using Zenject;

public class PlayerUnitIdentifier : IUnitIdentifier
{
    public PlayerUnitIdentifier(string unitId, int combatGroupId)
    {
        UnitId = unitId;
        CombatGroupId = combatGroupId;
    }

    public string UnitId { get; }
    public int CombatGroupId { get; }
}

public class PlayerDisplayDataProvider : IUnitDisplayDataProvider
{
    public string DisplayName => "Player";
}


public class PlayerMonoInstaller : MonoInstaller<PlayerMonoInstaller>
{
    public bool useKeyboard = true;

    public ReactiveProximityWatcher reactiveProximityWatcher;

    public CharacterClassSheet explicitCharacterSheet;

    public int combatGroupId = -1;

    public override void InstallBindings()
    {
#if !UNITY_EDITOR
        useKeyboard = false;
#endif

        if (explicitCharacterSheet)
        {
            Container.Bind<CharacterClassSheet>().FromInstance(explicitCharacterSheet);
        }
        else
        {
            //Todo: Implement Provider
            //Container.Bind<CharacterClassSheet>().FromResolve();
        }

        if (useKeyboard)
            Container.BindInterfacesAndSelfTo<KeyboardPlayerInputs>().AsSingle().NonLazy();

        Container.BindInterfacesAndSelfTo<UnitTransition>().FromComponentOnRoot().AsSingle().NonLazy();

        Container.BindInterfacesAndSelfTo<SpellSystem>().AsSingle().NonLazy();

        Container.Bind<IUnitIdentifier>().To<PlayerUnitIdentifier>()
            .FromInstance(new PlayerUnitIdentifier("player", combatGroupId));

        Container.Bind<IUnitDisplayDataProvider>().To<PlayerDisplayDataProvider>()
            .FromInstance(new PlayerDisplayDataProvider()).AsSingle();

        Container.Bind<IStatsProvider>().To<PlayerStatsProvider>().AsSingle();


        Container.BindInterfacesAndSelfTo<ReactiveProximityWatcher>().FromInstance(reactiveProximityWatcher)
            .AsSingle();

        Container.Bind<IUnitInteractionSystem>().To<PlayerInteractionSystem>().AsSingle().NonLazy();
        
        Container.BindInterfacesAndSelfTo<PlayerBrain>().AsSingle();
    }
}