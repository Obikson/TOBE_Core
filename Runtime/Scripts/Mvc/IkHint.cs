﻿using System;
using UnityEngine;

namespace Tobe.Core.Mvc
{
    [Serializable]
    public class IkHint
    {
        public Transform positionTarget;

        [Range(0, 1)] public float positionWeight;

        public AvatarIKHint ikHint;

        public void Apply(Animator animator)
        {
            if (positionTarget)
            {
                animator.SetIKHintPosition(ikHint, positionTarget.position);
                animator.SetIKHintPositionWeight(ikHint, positionWeight);
            }
            else
            {
                animator.SetIKHintPositionWeight(ikHint, 0);
            }
        }
    }
}