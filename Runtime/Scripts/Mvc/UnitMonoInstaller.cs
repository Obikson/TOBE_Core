﻿using System;
using ModestTree;
using Tobe.Core;
using Tobe.Core.Mvc;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

[Serializable]
public class UnitContainerContext
{
    public Transform projectileSpawner;
    public Transform root;
}

public class UnitMonoInstaller : MonoInstaller<UnitMonoInstaller>
{
    public ItemSlotRig itemSlotRig;
    public UnitBodyAnimator unitBodyAnimator;
    

    [SerializeField] private UnitContainerContext unitContainerContext;
    [SerializeField] private LocomotionTweak locomotionTweak;

    public override void InstallBindings()
    {
        Container.Bind<LocomotionTweak>().FromInstance(locomotionTweak).AsSingle();

        Container.Bind<UnitContainerContext>().FromInstance(unitContainerContext).AsSingle();

        Container.BindInterfacesAndSelfTo<UnitInventorySystem>().AsSingle();


        Container.BindInterfacesAndSelfTo<UnitMonoProxy>().FromComponentOnRoot().AsSingle().NonLazy();

        Container.BindInterfacesAndSelfTo<ItemSystem>().AsSingle();

        Container.Bind<Rigidbody>().FromComponentOnRoot().AsSingle();

        Container.BindInterfacesAndSelfTo<ItemSlotRig>().FromInstance(itemSlotRig).AsSingle();

        Container.BindInterfacesAndSelfTo<Transform>().FromInstance(unitContainerContext.root).AsSingle();

        Container.BindInterfacesAndSelfTo<UnitBodyAnimator>().FromInstance(unitBodyAnimator).AsSingle();


        Container.BindInterfacesAndSelfTo<StatsSystem>().AsSingle().NonLazy();


        Container.DeclareSignal<ItemStashSignal>();
        Container.DeclareSignal<DamageSignal>();
    }
}