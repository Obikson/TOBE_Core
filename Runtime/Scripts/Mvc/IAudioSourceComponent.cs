﻿using UnityEngine;

namespace Tobe.Core.Mvc
{
    public interface IAudioSourceComponent : IViewComponent
    {
        void PlayOneShot(AudioClip clip);
        void PlayOneShot(AudioClip clip, float volumeScale);
        void Play();
        void Play(AudioClip clip);

        void SetVolume(float volumeScale);
        void Stop();

        void Pause();
        void UnPause();
    }
}