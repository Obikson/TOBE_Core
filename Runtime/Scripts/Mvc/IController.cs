﻿using System;
using Tobe.Core.Abstractions;

namespace Tobe.Core.Mvc
{
    public interface IController<in TView> : IDisposable
    {
        void BindView(TView view);
    }

    public interface IController<in TView, in TViewModel> : IController<TView>, IDirty
    {
        void BindViewModel(TViewModel model);
    }
}