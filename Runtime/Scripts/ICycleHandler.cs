﻿using System;
using Zenject;

namespace Tobe.Core
{
    public interface ICycleHandler<in TCycleContext> : ICycleHandler
    {
        void OnCycleStarted(TCycleContext cycleContext);
        
        void OnCycleEnded(TCycleContext cycleContext);
    }

    public interface ICycleHandler : IInitializable, IDisposable
    {
        void OnCycleStarted();

        void OnCycleEnded();
    }
}