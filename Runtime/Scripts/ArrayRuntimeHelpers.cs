﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Tobe.Core
{
    public static class ArrayRuntimeHelpers
    {
        public static void PoplatePocoArray<TItem>(this TItem[] source, Action<int, TItem> initAction) where TItem: new()
        {
            for (int i = 0; i < source.Length; i++)
            {
                var item = source[i] = new TItem();
                
                initAction(i, item);
            }
        }
    }

    public static class TransformRuntimeHelpers
    {
        
        public static Transform FindChild(this Transform transform, Expression<Func<string, bool>> search)
        {
            var result = search.Compile();

            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                if (result.Invoke(child.name))
                {
                    return child;
                }
            }

            return null;
        }
        public static Transform FindDirectChild(this Transform transform, Regex regex)
        {
            return transform.Cast<Transform>().FirstOrDefault(childTransform => regex.IsMatch(childTransform.name));
        }
    }
}