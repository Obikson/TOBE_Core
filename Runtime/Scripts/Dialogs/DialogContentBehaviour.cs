﻿using System;
using UnityEngine;
using UnityEngine.Playables;

//The Data
[Serializable]
public class DialogContentBehaviour : PlayableBehaviour
{
    public string text;
    public Sprite avatar;
    public string npcName;

    // [Tooltip("Primary is on left, secondary is on right")]
    // public bool isPrimaryCharacter;


    [HideInInspector] public TimelineDialogManager timelineDialogManager;

    public DialogContentBehaviour()
    {
    }

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        if (timelineDialogManager.dialogText && !string.IsNullOrEmpty(text))
        {
            timelineDialogManager.dialogText.text = text;
        }

        if (timelineDialogManager.npcAvatar && avatar)
        {
            timelineDialogManager.npcAvatar.sprite = avatar;
        }

        if (timelineDialogManager.npcNameLabel && !string.IsNullOrEmpty(npcName))
        {
            timelineDialogManager.npcNameLabel.text = npcName;
        }
    }
}