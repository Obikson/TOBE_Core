﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackClipType(typeof(DialogClip))]
[TrackBindingType(typeof(TimelineDialogManager))]
public class DialogTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        // before building, update the binding field in the clips assets;
        var director = go.GetComponent<PlayableDirector>();

        var binding = director.GetGenericBinding(this);

        foreach (var c in GetClips())
        {
            var myAsset = c.asset as DialogClip;
            if (myAsset != null)
                myAsset.template.timelineDialogManager = binding as TimelineDialogManager;
        }

        return base.CreateTrackMixer(graph, go, inputCount);
    }
}