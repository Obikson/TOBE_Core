﻿using UnityEngine;
using Zenject;

namespace Tobe.Core
{
    public class RepositoryComplexInstaller : MonoInstaller<RepositoryComplexInstaller>
    {
        [SerializeField]
        private ItemRepository itemRepository;
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<ItemRepository>().FromInstance(itemRepository).AsSingle();
        }
    }
}