﻿namespace Tobe.Core
{
    public enum DisplayState
    {
        None,
        Block,
        Disabled
    }
}