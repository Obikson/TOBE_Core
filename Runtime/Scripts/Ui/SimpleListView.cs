﻿using System;
using System.Collections.Generic;
using Tobe.Core.Abstractions;

namespace Tobe.Core
{
    public class SimpleListView : ListView<SimpleListItem>
    {
        public void BindText(IEnumerable<string> data)
        {
            BindData(data, (str, item) =>
            {
                item.SetText(str);
            });
        }
    }
}