﻿using System;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using Zenject;

namespace Tobe.Core
{
    [Serializable]
    public class DisplayStateProperty : ReactiveProperty<DisplayState>
    {
    }

    [RequireComponent(typeof(CanvasGroup))]
    public class ContainerWindow : MonoBehaviour, IDisposable
    {
        private CanvasGroup _canvasGroup;


        [SerializeField] private WindowState initialState;

        [SerializeField] private WindowState currentState;

        [SerializeField] private DisplayState display;
        
        private DisplayStateProperty _display = new DisplayStateProperty();

        [Serializable]
        private struct WindowState
        {
            [ReadOnly] public bool blockRaycast;
            [ReadOnly] public bool interactable;
            [ReadOnly] public float alpha;
        }

        public DisplayStateProperty Display
        {
            get => _display;
            private set => _display = value;
        }

        [Inject]
        private void Ctor()
        {
            print("Ctor");

            _canvasGroup = GetComponent<CanvasGroup>();

            Display = new DisplayStateProperty();

            Display.Subscribe(x =>
            {
                display = x;
                RenderViewModel();
            });

            currentState = initialState = new WindowState
            {
                alpha = _canvasGroup.alpha,
                interactable = _canvasGroup.interactable,
                blockRaycast = _canvasGroup.blocksRaycasts
            };

            display = gameObject.activeInHierarchy ? DisplayState.Block : DisplayState.None;
            
            Display.Value = display;
        }

        public void SetDisplay(int value)
        {
            SetDisplay((DisplayState) value);
        }
        
        public void SetDisplay(DisplayState value)
        {
            Display.Value = value;
        }

        [Button]
        public void RenderViewModel()
        {
            gameObject.SetActive(display != DisplayState.None);

            switch (display)
            {
                case DisplayState.Block:
                    currentState.alpha = 1;
                    currentState.interactable = currentState.blockRaycast = true;
                    break;
                case DisplayState.None:
                    currentState.alpha = 0;
                    currentState.interactable = currentState.blockRaycast = false;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _canvasGroup.alpha = currentState.alpha;
            _canvasGroup.interactable = currentState.interactable;
            _canvasGroup.blocksRaycasts = currentState.blockRaycast;
        }


        public void Dispose()
        {
            Display?.Dispose();
        }
    }
}