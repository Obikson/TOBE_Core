﻿namespace Tobe.Core
{
    public enum ConfirmDialogOption
    {
        Abort,
        Confirm
    }
}