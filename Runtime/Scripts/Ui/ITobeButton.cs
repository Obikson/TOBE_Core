﻿using UnityEngine;
using UnityEngine.UI;

public interface ITobeButton
{
    Button.ButtonClickedEvent Click { get; }
    ITobeLabel Label { get; set; }

    bool Interactable { get; set; }
}

public interface ITobeLabelValue
{
    ITobeLabel Label { get; set; }
    ITobeLabel Value { get; set; }
}

public interface ITobeValueIcon
{
    Sprite Icon { get; set; }
    
    ITobeLabel Value { get; set; }
}