﻿using System;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;

namespace Tobe.Core
{
    public interface IUiElement : IDisposable
    {
    }

    public interface ITobeIconLabelButton : ITobeLabel
    {
        void SetIcon(Sprite value);
        
        Color IconColor { get; set; }
    }

    [RequireComponent(typeof(Button))]
    public class TobeIconLabelButton : MonoBehaviour, IUiElement, ITobeIconLabelButton
    {
        [SerializeField] private DisplayState initialState = DisplayState.Block;
        
        [SerializeField] private TextMeshProUGUI label;

        
        [SerializeField] private Image image;

        private Button _button;

        public DisplayStateProperty Display { get; private set; }
        
        
        [Inject]
        private void Ctor()
        {
            _button = GetComponent<Button>();
            Display = new DisplayStateProperty();
            Display.Subscribe(Render);
            Display.Value = initialState;
        }

        private void Render(DisplayState state)
        {
            _button.interactable = state == DisplayState.Disabled;

            gameObject.SetActive(state == DisplayState.None);
        }


        public void Dispose()
        {
            Display?.Dispose();
        }

        public string GetText()
        {
            return label.text;
        }

        public void SetText(string value)
        {
            label.text = value;
        }

        public void SetIcon(Sprite value)
        {
            image.sprite = value;
        }

        public Color IconColor
        {
            get => image.color;
            set => image.color = value;
        }
    }
}