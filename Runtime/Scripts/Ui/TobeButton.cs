﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Button))]
[ZenjectAllowDuringValidation]
public class TobeButton : MonoBehaviour, ITobeButton, IDisposable
{
    private Button button;

    public Button.ButtonClickedEvent Click
    {
        get
        {
            if (!button)
                button = GetComponent<Button>();
            return button.onClick;
        }
    }

    public ITobeLabel Label { get; set; }

    public bool Interactable
    {
        get => button.interactable;
        set => button.interactable = value;
    }

    public Graphic Background { get; private set; }

    [SerializeField] private bool explicitLabel;

    [EnableIf(nameof(explicitLabel))] public TobeLabel label;

    [Inject]
    private void Ctor()
    {
        button = GetComponent<Button>();

        Background = button.targetGraphic;
        
        if (explicitLabel)
        {
            Label = label;
        }
        else
        {
            Label = GetComponentInChildren<TobeLabel>() ??
                    transform.GetChild(0)?.gameObject.AddComponent<TobeLabel>();
        }
    }
    
    public void SetText(string value)
    {
        if (Label == null)
        {
            label.SetText(value);
        }
        else
        {
            Label.SetText(value);
        }
    }

    public string GetText()
    {
        return Label.GetText();
    }

    public void Dispose()
    {
        label?.Dispose();
    }
}