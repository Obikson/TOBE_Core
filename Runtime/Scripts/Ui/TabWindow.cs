﻿using System;
using System.Linq;
using ModestTree;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Tobe.Core
{
    [ZenjectAllowDuringValidation]
    public class TabWindow : MonoBehaviour, IUiElement, IValidatable
    {
        private TabPage[] _contentPages;

        public TobeButton tabPrefab;

        [SerializeField] private Transform container;

        [SerializeField] private Transform header;

        [SerializeField] private string defaultId;

        private TobeButton[] _headerButtons;

        public string ActiveTabId { get; private set; }

        private IInstantiator _instantiator;
        
        [Inject]
        private void Ctor(IInstantiator diContainer)
        {
            _instantiator = diContainer;
        }

        private void Start()
        {
            _contentPages = container.GetComponentsInChildren<TabPage>();

            _headerButtons = new TobeButton[_contentPages.Length];

            header.ClearChildren();

            var index = 0;
            foreach (var contentPage in _contentPages)
            {
                var item = _instantiator.InstantiatePrefabForComponent<TobeButton>(tabPrefab, header);

                item.SetText(contentPage.title);

                item.Click.AsObservable().Subscribe(x =>
                {
                    ActiveTabId = contentPage.id;
                    RenderView();
                });

                _headerButtons[index] = item;

                index++;
            }

            if (string.IsNullOrEmpty(defaultId))
                defaultId = _contentPages[0].id;

            ActiveTabId = defaultId;
            
            RenderView();
        }

        public void HideAllTabs()
        {
            foreach (var contentPage in _contentPages)
            {
                contentPage.gameObject.SetActive(false);
            }

            foreach (var headerButton in _headerButtons)
            {
                if (!headerButton.Background) continue;

                Color.RGBToHSV(headerButton.Background.color, out var h, out var s, out var v);

                var vaue = .75f; //s - .2f;

                headerButton.Background.color = Color.HSVToRGB(h, s, vaue);
            }
        }

        public void ShowTab(string id)
        {
            ActiveTabId = id;
            RenderView();
        }

        private void RenderView()
        {
            HideAllTabs();

            var page = _contentPages.FirstOrDefault(x => x.id.Equals(ActiveTabId));

            if (page)
            {
                var index = _contentPages.IndexOf(page);

                var color = _headerButtons[index].Background.color;

                
                Color.RGBToHSV(color, out var h, out var s, out var v);

                //TODO: cache v
                _headerButtons[index].Background.color = Color.HSVToRGB(h, s, 1);
                
                page.gameObject.SetActive(true);
            }
        }

        public void Dispose()
        {
        }

        public void Validate()
        {
            Assert.That(tabPrefab != null, "tabPrefab != null");
        }
    }
}