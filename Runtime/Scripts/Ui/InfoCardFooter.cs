﻿using UnityEngine;
using UnityEngine.UI;

namespace Tobe.Core
{
    public class InfoCardFooter : MonoBehaviour
    {
        [SerializeField] private Image background;
        
        public void SetBackgroundColor(Color color)
        {
            background.color = color;
        }
    }
    
}

