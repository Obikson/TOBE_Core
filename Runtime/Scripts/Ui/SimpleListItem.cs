﻿using System;
using TMPro;
using UnityEngine;


namespace Tobe.Core
{
    public class SimpleListItem : MonoBehaviour, IDisposable
    {

        [SerializeField]
        private TextMeshProUGUI label;
        
        public void SetText(string text)
        {
            label.text = text;
        }

        public void Dispose()
        {
        }
    }
}