﻿public interface ITobeLabel
{
    string GetText();
    
    void SetText(string value);
}