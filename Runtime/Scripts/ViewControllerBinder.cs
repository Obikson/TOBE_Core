﻿using UnityEngine;
using Tobe.Core.Mvc;
using Zenject;

namespace Tobe.Core
{
    public class ViewControllerBinder<TView, TController> where TController : IController<TView>
    {
        private readonly DiContainer _container;

        public ViewControllerBinder(DiContainer container)
        {
            _container = container;
        }

        public void WithControllerImplementation<TControllerImpl>() where TControllerImpl : class, TController
        {
            _container.BindInterfacesAndSelfTo<TControllerImpl>().AsTransient();
        }
        
        public void WithControllerImplementationAsSingle<TControllerImpl>() where TControllerImpl : class, TController
        {
            _container.BindInterfacesAndSelfTo<TControllerImpl>().AsSingle();
        }
    }
}