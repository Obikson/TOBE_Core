﻿using System;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

public class ModularCharacterRigRepository
{
    private const string ModularLayoutKeyPrefix = "modular.layout";

    private readonly string[] _partsSerialized =
    {
        "Head",
        "Eyebrows",
        "FacialHair",
        "Hair"
    };

    private PlayerCharacterProfilesRepository _playerCharacterProfilesRepository;
    
    public void SaveLayout(GenderRoot male, GenderRoot all, string guid)
    {
        if (string.IsNullOrEmpty(guid)) throw new NullReferenceException();

        var layout = new ModularCharacterRigLayout
        {
            Gender = male.Gender,
            PartIds = male.Containers.Union(all.Containers).Where(x => _partsSerialized.Contains(x.PartTag))
                .ToDictionary(x => x.PartTag, y => y.CurrentPartIndex)
        };

        var jsonVal = JsonConvert.SerializeObject(layout);

        PlayerPrefs.SetString($"{ModularLayoutKeyPrefix}.{guid}", jsonVal);
    }

    public ModularCharacterRigLayout LoadLayout(string guid)
    {
        if (string.IsNullOrEmpty(guid)) return null;

        var jsonVal = PlayerPrefs.GetString($"{ModularLayoutKeyPrefix}.{guid}");

        if (string.IsNullOrEmpty(jsonVal))
            return null;

        var layout = JsonConvert.DeserializeObject<ModularCharacterRigLayout>(jsonVal);

        return layout;
    }

 
}