﻿using System;

public static class GenderEnumHelpers
{
    public static string ToModularString(this Gender gender)
    {
        switch (gender)
        {
            case Gender.Male:
                return "Male";
            case Gender.Female:
                return "Female";
            case Gender.AllGender:
                return "All";
            default:
                throw new ArgumentOutOfRangeException(nameof(gender), gender, null);
        }
    }
}