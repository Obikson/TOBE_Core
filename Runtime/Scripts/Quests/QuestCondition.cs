﻿using System;
using Sirenix.OdinInspector;
using Tobe.Core;
using UnityEngine;
using Object = UnityEngine.Object;

public interface IQuestTrigger
{
    string Id { get; }
}

[Serializable]
public class QuestCondition
{
    [DisableIf(nameof(conditionObject))]
    public QuestConditionType type;

    private void CheckObjectType()
    {
        if (conditionObject is NpcMetadata npcMetadata)
        {
            type = QuestConditionType.UnitKill;
            targetId = npcMetadata.npcId;
        }
        else if (conditionObject is GameObject gameObject)
        {
            var stashableItem = gameObject.GetComponent<IStashableItem>();
            var questTrigger = gameObject.GetComponent<IQuestTrigger>();
            if (stashableItem != null)
            {
                type = QuestConditionType.Item;
                itemId = stashableItem.Id;
            }

            else if (questTrigger != null)
            {
                type = QuestConditionType.Trigger;
                triggerId = questTrigger.Id;
            }
        }
    }

    [HideIf(nameof(type), QuestConditionType.Resource), OnValueChanged(nameof(CheckObjectType))]
    public Object conditionObject;

    [DisableIf(nameof(conditionObject)), ShowIf(nameof(type), QuestConditionType.Item)]
    public int itemId;

    [DisableIf(nameof(conditionObject)), ShowIf(nameof(type), QuestConditionType.Trigger)]
    public string triggerId;

    [ShowIf(nameof(type), QuestConditionType.Resource)]
    public ResourceItem resourceItem;

    [DisableIf(nameof(conditionObject)),ShowIf(nameof(type), QuestConditionType.UnitKill)]
    public string targetId;

    public int amount;

    public string title;
}

public enum QuestConditionType
{
    Unknown,
    Trigger,
    Item,
    Resource,
    UnitKill
}