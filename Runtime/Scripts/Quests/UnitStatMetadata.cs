﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "Player/Character Stats")]
public class UnitStatMetadata : ScriptableObject
{
    public string id = "new_stat";
    public string displayName = "New Stat";

    [SerializeField] private StatType type;

    [PreviewField(100, ObjectFieldAlignment.Center)]
    public Sprite icon;

    public Color color = Color.black;

    [TextArea(20, 100)] public string description;

    public enum StatType
    {
        FloatingNumber
    }
}