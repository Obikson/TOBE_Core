﻿public enum DialogOptionType
{
    LeadToAnotherDialog,
    CustomEventHandler,
    CustomFunctionCall
}