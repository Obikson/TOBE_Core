﻿using UnityEngine;
using Zenject;
using UniRx;

public static class Vector2RuntimeHelpers
{
    public static Vector2 Clamp01(this Vector2 source)
    {
        return Clamp(source, 0, 1);
    }

    public static Vector2 Clamp(this Vector2 source, Vector2 min, Vector2 max)
    {
        source[0] = Mathf.Clamp(source[0], min.x, max.x);
        source[1] = Mathf.Clamp(source[1], min.y, max.y);

        return source;
    }

    public static Vector2 Clamp(this Vector2 source, float min, float max)
    {
        source[0] = Mathf.Clamp(source[0], min, max);
        source[1] = Mathf.Clamp(source[1], min, max);

        return source;
    }
}

public class GestureTransformHandler : MonoBehaviour
{
    public TouchSystem touchSystem;

    public float rotationMultiplier = 1f;

    public Vector3 rotationVector = Vector3.up;

    public bool localRotation = true;

    [Inject]
    private void Ctor()
    {
    }

    public void Awake()
    {
        var max = Vector2.one;

        var min = Vector2.one * -1;

        touchSystem.Delta.Select(x => x.Clamp(min, max)).Subscribe(RotationLogic);

        touchSystem.Velocity
            .Where(x => touchSystem.Style.Value == TouchSystem.PointerStyle.Fling)
            .Subscribe(RotationLogic);
    }


    private void RotationLogic(Vector2 velocity)
    {
        var rot = velocity * rotationMultiplier * Time.deltaTime;

        transform.rotation *= Quaternion.Euler(new Vector3(0, rot.x, 0));
    }
}