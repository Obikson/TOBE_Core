﻿using System;
using TMPro;
using UniRx;
using UnityEngine;
using Zenject;

public class TouchSystem : MonoBehaviour
{
    [SerializeField]
    private Vector2 velocity;

    public float decayFactor = 10f;

    public Vector2 maxVelocity = Vector2.one;

    public float pointerUpToFling = 10f;

    public ReactiveProperty<PointerStyle> Style { get; private set; }

    private Vector2ReactiveProperty _velocity;

    public IReadOnlyReactiveProperty<Vector2> Velocity => _velocity;

    public IReadOnlyReactiveProperty<Vector2> Delta => _delta;

    private Vector2ReactiveProperty _delta;

    [Inject]
    private void Ctor()
    {
        _delta = new Vector2ReactiveProperty();
        _velocity = new Vector2ReactiveProperty();

        Style = new ReactiveProperty<PointerStyle>();
    }

    public void Awake()
    {
    }

    public float touchDownTime;

    public AnimationCurve decayCurve = AnimationCurve.Linear(0, 0, 1, 1);

    public enum PointerStyle
    {
        None,
        Drag,
        Fling
    }

    private void Start()
    {
        var trigger = gameObject.AddComponent<ObservablePointerDownTrigger>();

        var triggerUp = gameObject.AddComponent<ObservablePointerUpTrigger>();


        trigger.OnPointerDownAsObservable().Subscribe(eventData =>
        {
            Style.Value = PointerStyle.Drag;

            var increaseAmountX = eventData.delta.x;
            var increaseAmountY = eventData.delta.y;

            velocity[0] = Mathf.Clamp(increaseAmountX, -maxVelocity.x, maxVelocity.x);

            velocity[1] = Mathf.Clamp(increaseAmountY, -maxVelocity.y, maxVelocity.y);

            touchDownTime = Mathf.Clamp01(touchDownTime + Time.deltaTime);

            _delta.Value = eventData.delta;

            _velocity.Value = velocity;
        });

        triggerUp.OnPointerUpAsObservable().Subscribe(eventData =>
        {
            if (Style.Value == PointerStyle.Drag)
            {
                Style.Value = eventData.delta.magnitude >= pointerUpToFling ? PointerStyle.Fling : PointerStyle.None;
            }

            touchDownTime = Mathf.Clamp01(touchDownTime - Time.deltaTime);

            var reduceAmount = decayFactor * Time.deltaTime * (1 - decayCurve.Evaluate(touchDownTime));

            var velocityX = velocity[0];

            var velocityY = velocity[1];

            velocity[0] = Mathf.Clamp01(velocityX > 0 ? velocityX - reduceAmount :
                velocityX < 0 ? velocityX + reduceAmount : 0);

            velocity[1] = Mathf.Clamp01(velocityY > 0 ? velocityY - reduceAmount :
                velocityY < 0 ? velocityY + reduceAmount : 0);


            if (Mathf.Abs(velocityX) < 0.1f)
            {
                velocity[0] = 0;
            }

            if (Mathf.Abs(velocityY) < 0.1f)
            {
                velocity[1] = 0;
            }

            _velocity.Value = velocity;
        });
    }
}