﻿using System;
using System.Linq;
using Tobe.Core;
using Tobe.Core.Mvc.Implementations;
using UniRx;
using UnityEngine;
using Zenject;


public class DialogWindowController : Controller<DialogWindowView, DialogWindowViewModel>, IDialogWindowController
{
    private int _index;

    private readonly IQuestManager _questManager;

    private readonly IDialogFunctionsContext _dialogFunctionsContext;

    private DialogContext _currentDialogContext;


    public DialogWindowController(IQuestManager questManager, IDialogFunctionsContext dialogFunctionsContext)
    {
        _dialogComplete = new Subject<DialogContext>();
        _dialogFunctionsContext = dialogFunctionsContext;
        _questManager = questManager;
    }

    public override void BindView(DialogWindowView view)
    {
        base.BindView(view);
        View.nextButton.Click.AddListener(NextButtonClick);

        View.cancelButton.Click.AddListener(CancelButtonClick);
    }

    public override void BindViewModel(DialogWindowViewModel model)
    {
        base.BindViewModel(model);


#if UNITY_EDITOR
        if (_currentDialogContext != null)
            RenderDialogLine(_currentDialogContext.DialogLines[0]);
#endif
    }

    private void CancelButtonClick()
    {
        CancelDialog();
    }

    private void NextButtonClick()
    {
        Next();
    }


    public void Next()
    {
        _index++;

        if (_index >= _currentDialogContext.DialogLines.Length)
        {
            CancelDialog();
            return;
        }

        RenderDialogLine(_currentDialogContext.DialogLines[_index]);
    }

    private void RenderDialogLine(DialogLine line)
    {
        View.npcName.SetText(_currentDialogContext.DisplayDataProvider.DisplayName);

        View.nextButton.gameObject.SetActive(true);

        if (_index == _currentDialogContext.DialogLines.Length - 1)
        {
            View.nextButton.SetText(ViewModel.nextButtonCompleteText);
        }
        else
        {
            View.nextButton.SetText(ViewModel.nextButtonNextText);
        }

        switch (line.type)
        {
            case DialogType.Text:
                View.textLabel.gameObject.SetActive(true);
                View.inDialogListView.gameObject.SetActive(false);
                RenderTextDialogLine(line);
                break;
            case DialogType.QuestChoice:
                var hasAnyQuests = line.quests.Any(x =>
                    _questManager.CanAcceptQuest(x.id) || _questManager.CanCollectReward(x.id));

                if (!hasAnyQuests)
                {
                    View.nextButton.SetText(ViewModel.nextButtonNextText);
                    View.nextButton.gameObject.SetActive(true);

                    View.textLabel.SetText(line.allQuestsTaken);
                    View.textLabel.gameObject.SetActive(true);

                    View.inDialogListView.gameObject.SetActive(false);
                }
                else
                {
                    View.textLabel.gameObject.SetActive(true);
                    View.nextButton.gameObject.SetActive(false);

                    View.inDialogListView.gameObject.SetActive(true);
                    RenderTextDialogOptions(line);
                }

                break;
            case DialogType.CustomOptions:
            {
                if (!string.IsNullOrEmpty(line.text))
                {
                    View.textLabel.SetText(line.text);

                    View.textLabel.gameObject.SetActive(true);
                }
                else
                {
                    View.textLabel.gameObject.SetActive(false);
                }


                View.nextButton.gameObject.SetActive(false);

                View.inDialogListView.gameObject.SetActive(true);

                View.inDialogListView.BindData(line.dialogRerouteOptions, (option, button) =>
                {
                    button.SetText(option.text);

                    button.Click.AddListener(() =>
                    {
                        switch (option.optionType)
                        {
                            case DialogOptionType.LeadToAnotherDialog:

                                RenderDialog(new DialogContext
                                {
                                    DialogLines = new[] {option.dialog}
                                });
                                break;
                            case DialogOptionType.CustomEventHandler:
                                option.onOptionSelected?.Invoke(option);
                                break;
                            case DialogOptionType.CustomFunctionCall:
                                switch (option.methodParam)
                                {
                                    case MethodParam.None:
                                        _dialogFunctionsContext?.SendMessage(option.methodName, null,
                                            SendMessageOptions.RequireReceiver);
                                        break;
                                    case MethodParam.UnityObject:
                                        _dialogFunctionsContext?.SendMessage(option.methodName, option.objectValue,
                                            SendMessageOptions.RequireReceiver);
                                        break;
                                    case MethodParam.StringValue:
                                        _dialogFunctionsContext?.SendMessage(option.methodName, option.stringValue,
                                            SendMessageOptions.RequireReceiver);
                                        break;
                                    case MethodParam.NumberValue:
                                        _dialogFunctionsContext?.SendMessage(option.methodName, option.numberValue,
                                            SendMessageOptions.RequireReceiver);
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException();
                                }

                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    });
                });
                break;
            }
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void RenderTextDialogOptions(DialogLine line)
    {
        View.textLabel.SetText(line.text);
        View.inDialogListView.BindData(line.quests, (data, button) =>
        {
            button.SetText(_questManager.CanCollectReward(data.id) && data.questRewardResources != null &&
                           data.questRewardResources.Length > 0
                ? "Reward: " + data.title
                : data.title);

            button.Click.AddListener(() =>
            {
                if (_questManager.CanCollectReward(data.id))
                {
                    _questManager.CollectReward(data.id);
                    if (data.completionDialog != null)
                    {
                        RenderDialogLine(data.completionDialog);
                    }
                }
                else
                {
                    View.nextButton.gameObject.SetActive(true);

                    _questManager.TakeQuest(data);

                    View.inDialogListView.gameObject.SetActive(false);

                    View.textLabel.SetText(data.description);

                    View.textLabel.gameObject.SetActive(true);
                }
            });

            button.Interactable = _questManager.CanAcceptQuest(data.id) || _questManager.CanCollectReward(data.id);
        });
    }

    private void RenderTextDialogLine(DialogLine line)
    {
        View.textLabel.SetText(line.text);
    }

    public void RenderDialog(DialogContext dialogContext)
    {
        if (_currentDialogContext != null)
        {
            CancelDialog();
        }

        _currentDialogContext = dialogContext;

        RenderDialogLine(dialogContext.DialogLines[_index]);
    }


    public void CancelDialog()
    {
        _dialogComplete?.OnNext(_currentDialogContext);

        _currentDialogContext = null;

        _index = 0;

        Close();
    }

    public IObservable<DialogContext> OnClosing => _dialogComplete;

    private Subject<DialogContext> _dialogComplete;

    public void Open()
    {
        View.root.gameObject.SetActive(true);
    }

    public void Close()
    {
        View.root.gameObject.SetActive(false);
    }
}