﻿using Zenject;

public class CharacterClassSheetRepositoryMonoInstaller : MonoInstaller<CharacterClassSheetRepositoryMonoInstaller>
{
    public CharacterClassSheetRepository repository;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<CharacterClassSheetRepository>().FromInstance(repository).AsSingle();
    }
}