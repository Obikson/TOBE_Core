﻿using System.Collections.Generic;
using UnityEngine;

public class ModularHeadPartsContainer : IModularPart
{
    private bool _hasFacialsVisible;
    public IModularPart NoElements { get; }

    public IModularPart AllElements { get; }
    public string PartTag { get; }

    public int Index { get; }
    public IEnumerable<GameObject> Items => HasFacialsVisible ? NoElements.Items : AllElements.Items;

    public void ShowNext()
    {
        if (HasFacialsVisible)
        {
            NoElements.ShowNext();
        }
        else
        {
            AllElements.ShowNext();
        }
    }

    public void ShowPrev()
    {
        if (HasFacialsVisible)
        {
            NoElements.ShowPrev();
        }
        else
        {
            AllElements.ShowPrev();
        }
    }

    public void Show()
    {
        if (HasFacialsVisible)
        {
            NoElements.Show();
        }
        else
        {
            AllElements.Show();
        }
    }

    public void Hide()
    {
        if (_hasFacialsVisible)
        {
            NoElements.Hide();
        }
        else
        {
            AllElements.Hide();
        }
    }

    public int CurrentPartIndex
    {
        get => _hasFacialsVisible ? NoElements.CurrentPartIndex : AllElements.CurrentPartIndex;
        set
        {
            if (_hasFacialsVisible)
            {
                NoElements.CurrentPartIndex = value;
            }
            else
            {
                AllElements.CurrentPartIndex = value;
            }
        }
    }

    public bool HasFacialsVisible
    {
        get => _hasFacialsVisible;
        set
        {
            _hasFacialsVisible = value;
            if (_hasFacialsVisible)
            {
                NoElements.Show();
                AllElements.Hide();
            }
            else
            {
                AllElements.Show();
                NoElements.Hide();
            }
        }
    }

    public ModularHeadPartsContainer(Gender gender, GameObject root, string prefixTag)
    {
        PartTag = prefixTag;

        var gameObject = root.transform.Find($"{gender.ToModularString()}_00_Head");

      
        Index = 0;

        NoElements =
            new ModularPartsContainer(gameObject.transform.Find($"{gender.ToModularString()}_Head_No_Elements")
                .gameObject, -1);
        AllElements =
            new ModularPartsContainer(gameObject.transform.Find($"{gender.ToModularString()}_Head_All_Elements")
                .gameObject);

        HasFacialsVisible = false;
    }
}