﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PatrolPointMonoInstaller : MonoInstaller<PatrolPointMonoInstaller>
{
    [SerializeField] private PatrolPoint[] patrolPoints;

    public override void InstallBindings()
    {
        Container.Bind<IEnumerable<PatrolPoint>>().FromInstance(patrolPoints).AsSingle();
    }
}