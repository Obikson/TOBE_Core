﻿using System;


[Serializable]
public class PlayerQuestData
{
    public string QuestId { get; set; }
    
    public bool IsCompleted { get; set; }
    
    public bool IsRewardCollected { get; set; }
    
    public PlayerQuestConditionPair[] QuestConditionPairs { get; set; }
}