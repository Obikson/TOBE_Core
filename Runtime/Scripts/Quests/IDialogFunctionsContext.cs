﻿using UnityEngine;

public interface IDialogFunctionsContext
{
    
    void SendMessage(string methodName, object data, SendMessageOptions options);
}