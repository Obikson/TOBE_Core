﻿using System;
using System.Collections.Generic;
using System.Linq;
using ModestTree;
using UnityEngine;

[CreateAssetMenu(fileName = "Quest Repository", menuName = "Repositories/Quests")]
public class QuestsRepository : ScriptableObject, IQuestRepository
{
    [SerializeField] private QuestData[] allQuests;

    public IEnumerable<QuestData> AllQuests => allQuests;

    private void OnValidate()
    {
        if (allQuests != null && allQuests.Any())
            Assert.That(allQuests.GroupBy(x => x.id).All(x => x.Count() == 1)
                , "allQuests.GroupBy(x=>x.id).All(x=>x.Count()==1)");
    }
}