﻿using System.Collections.Generic;
using UnityEngine;

public interface IModularPart
{
    string PartTag { get; }

    int Index { get; }
    IEnumerable<GameObject> Items { get; }
    void ShowNext();
    void ShowPrev();
    void Show();
    void Hide();

    int CurrentPartIndex { get; set; }
}