﻿using System;
using Tobe.Core.Mvc.Implementations;
using UnityEngine;

public class DialogWindow : MonoViewControllerProxy<DialogWindowViewModel, DialogWindowView, IDialogWindowController> ,IDialogWindowController
{
    [SerializeField] private DialogWindowView view;

    protected override DialogWindowView View => view;

    public void Open()
    {
        Controller.Open();
    }

    public void Close()
    {
        Controller.Close();
    }

    public void RenderDialog(DialogContext data)
    {
        Controller.RenderDialog(data);
    }

    public void CancelDialog()
    {
        Controller.CancelDialog();
    }

    public IObservable<DialogContext> OnClosing => Controller.OnClosing;
}