﻿using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

public class QuestConditionTrigger : MonoBehaviour
{
    [Title("Quests that will be resolved")]    public QuestData[] linkedQuests;

    [HideInInspector]
    [SerializeField] public QuestConditionPair[] conditionsResolved = new QuestConditionPair[0];

    private IQuestManager _questManager;
    
    [Inject]
    private void Ctor(IQuestManager questManager)
    {
        _questManager = questManager;
    }
    
    [Button]
    public void Fulfill()
    {
        foreach (var conditionPair in conditionsResolved)
        {
           var quest = _questManager.IncompleteQuests.FirstOrDefault(x => x.QuestId.Equals(conditionPair.questId));
           
           if(quest == null) continue;

           var condition = quest.QuestConditionPairs.FirstOrDefault(x => x.triggerId.Equals(conditionPair.conditionId));

           if (condition != null)
           {
               condition.collectedAmount += conditionPair.addAmount;
               _questManager.CheckQuestCompletion(quest.QuestId);
           }
        }
    }
}