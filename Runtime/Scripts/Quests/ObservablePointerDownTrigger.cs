﻿using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine.EventSystems;

public class ObservablePointerDownTrigger : ObservableTriggerBase, IPointerDownHandler, IPointerUpHandler
{
    Subject<PointerEventData> onLongPointerDown;

    private PointerEventData _lastPointerEventData;

    private bool _isDown;

    private void Update()
    {
        if (_isDown)
            onLongPointerDown?.OnNext(_lastPointerEventData);
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        _lastPointerEventData = eventData;
        _isDown = true;
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        _lastPointerEventData = eventData;
        _isDown = false;
    }

    public IObservable<PointerEventData> OnPointerDownAsObservable()
    {
        return onLongPointerDown ?? (onLongPointerDown = new Subject<PointerEventData>());
    }

    protected override void RaiseOnCompletedOnDestroy()
    {
        onLongPointerDown?.OnCompleted();
    }
}