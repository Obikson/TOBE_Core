﻿using System;

[Serializable]
public class QuestConditionPair
{
    public string questId;
    public string conditionId;
    public int addAmount;
}

[Serializable]
public class PlayerQuestConditionPair
{
    public string questId;
    public string triggerId;
    public int collectedAmount;
    public string resourceId { get; set; }
    public int itemId { get; set; }
    public string targetId { get; set; }
}