﻿using Zenject;

public class QuestInstaller : Installer<QuestInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<PlayerPrefsManager>().AsSingle();
        Container.BindInterfacesAndSelfTo<QuestManager>().AsSingle();
    }
}