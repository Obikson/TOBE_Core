﻿using UnityEngine;

[CreateAssetMenu(menuName = "Player/Spell Sheet")]
public class SpellSheet : ScriptableObject
{
    public string id = "new_spell";
    public string displayName = "New Spell";
    public Sprite icon;
    public float costAmount;
    public UnitStatMetadata consumesStat;
    public SpellEffect[] effects;
}

