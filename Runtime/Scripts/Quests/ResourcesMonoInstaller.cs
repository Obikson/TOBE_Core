﻿using UnityEngine;
using Zenject;

public class ResourcesMonoInstaller : MonoInstaller<ResourcesMonoInstaller>
{
    [SerializeField]
    private ResourceRepository resourceRepository;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<ResourceRepository>().FromInstance(resourceRepository).AsSingle();

        Container.BindInterfacesAndSelfTo<PlayerResourcesManager>().AsSingle();
    }
}