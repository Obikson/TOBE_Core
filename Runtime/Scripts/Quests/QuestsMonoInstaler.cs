﻿using System;
using Zenject;

public class QuestsMonoInstaler : MonoInstaller<QuestsMonoInstaler>
{
    public QuestsRepository questsRepository;
    
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<QuestsRepository>().FromInstance(questsRepository).AsSingle();
        
        QuestInstaller.Install(Container);
    }
}