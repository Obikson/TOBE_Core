﻿using Sirenix.OdinInspector;
using Tobe.Core;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class SingleItemLoot : MonoBehaviour, IStashableItem
{
    public int Id { get; private set; }

    [ValidateInput(nameof(ValidateType), "Place Stashable item here"), OnValueChanged(nameof(ValueChanged))]
    public GameObject stashableItem;

    private void ValueChanged()
    {
        if (!stashableItem)
        {
            Id = 0;
            return;
        }

        var item = stashableItem.GetComponent<IStashableItem>();

        Id = item?.Id ?? 0;
    }

    private bool ValidateType(GameObject value)
    {
        if (value == null) return false;

        return value.GetComponent<IStashableItem>() != null;
    }


    public void Stash()
    {
        Destroy(gameObject);
    }

    public Vector3 WorldPosition => transform.position;
}