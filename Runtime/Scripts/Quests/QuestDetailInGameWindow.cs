﻿using Sirenix.OdinInspector;
using Tobe.Core.Abstractions;
using UnityEngine;
using Zenject;

// public class QuestListInGameWindow : ListView<>
// {
//     
// }

public class QuestDetailInGameWindow : ListView<ConditionsListItem>
{
    public TobeLabel description;

    public TobeButton acceptButton;

    
    public TobeLabel title;
    
    public TobeButton cancelButton;

    [SerializeField]
    private QuestData currentQuest;

    private IQuestManager _questManager;
    
    [Inject]
    private void Ctor(IQuestManager questManager)
    {
        _questManager = questManager;
    }

    private void Start()
    {
        acceptButton.Click.AddListener(AcceptQuest);
        cancelButton.Click.AddListener(CancelQuest);
    }

    public void SetCurrentQuest(QuestData questData)
    {
        currentQuest = questData;
        RenderQuestData();
    }

    [Button]
    public void RenderQuestData()
    {
        description.SetText(currentQuest.description);
        
        title.SetText(currentQuest.title);
        
        BindData(currentQuest.successConditions, (condition, item) =>
        {
            item.label.text = condition.title;
        });

        acceptButton.gameObject.SetActive(_questManager.CanAcceptQuest(currentQuest.id));
        
        Show();
    }


    private void AcceptQuest()
    {
        _questManager.TakeQuest(currentQuest);
        
    }


    private void CancelQuest()
    {
        currentQuest = null;
        Hide();
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    private void Show()
    {
        gameObject.SetActive(true);
        RenderQuestData();
    }
}