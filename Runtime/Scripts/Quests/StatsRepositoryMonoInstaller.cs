﻿using Zenject;

public class StatsRepositoryMonoInstaller : MonoInstaller<StatsRepositoryMonoInstaller>
{
    public StatsRepository statsRepository;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<StatsRepository>().FromInstance(statsRepository).AsSingle();
    }
}