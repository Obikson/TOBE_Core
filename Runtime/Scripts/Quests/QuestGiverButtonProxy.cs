﻿using UnityEngine;


public class QuestGiverButtonProxy : MonoBehaviour
{
    public QuestData currentQuest;

    public QuestDetailInGameWindow InGameWindow;
    
    public void GiveQuest()
    {
        InGameWindow.gameObject.SetActive(true);

        InGameWindow.SetCurrentQuest(currentQuest);
    }
}