﻿using System.Collections.Generic;

public class ModularCharacterRigLayout
{
    public Gender Gender { get; set; }
    public Dictionary<string, int> PartIds { get; set; }
}