﻿using UnityEngine;

public class ModularWearableSet : MonoBehaviour
{
    public string[] visibleGameObjectNames;

    public void Wear(ModularCharacterRig rig)
    {
        foreach (var visibleGameObjectName in visibleGameObjectNames)
        {
            rig.ShowPart(visibleGameObjectName);
        }
    }
    
}