﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "Player/Spell Effect")]
public class SpellEffect : ScriptableObject
{
    public bool isCastOnSelf;
    
    public SpellEffectType effectType;
    public string id;

    private bool EnableAdd => statsRepository && selectedStat;
    private bool ShowWarning => statsRepository == null;
    
    [InfoBox("Drag Stats repository here to edit character stats", InfoMessageType.Warning, nameof(ShowWarning))]
    [SerializeField] private StatsRepository statsRepository;

    [ValueDropdown(nameof(GetStats))] [LabelText("Select stat to add")] [SerializeField] [ShowIf(nameof(statsRepository))]
    private UnitStatMetadata selectedStat;
    
    private IEnumerable<UnitStatMetadata> GetStats()
    {
        return statsRepository.allKnownStats;
    }

    private FloatStat AddStat()
    {
        if (selectedStat != null && !string.IsNullOrEmpty(selectedStat.id))
        {
            return new FloatStat(selectedStat);
        }

        return null;
    }
    
    [ListDrawerSettings(CustomAddFunction = nameof(AddStat)), EnableIf(nameof(EnableAdd))]
    public FloatStat[] affectsStat;

    public GameObject projectile;
}

public enum SpellEffectType
{
    Instant,
    Self
}