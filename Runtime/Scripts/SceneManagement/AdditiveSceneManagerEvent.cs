﻿using System;
using UnityEngine.Events;

namespace Tobe.Core
{
    [Serializable]
    public class AdditiveSceneManagerEvent : UnityEvent<AdditiveSceneLoadGroup>
    {
        
    }
}