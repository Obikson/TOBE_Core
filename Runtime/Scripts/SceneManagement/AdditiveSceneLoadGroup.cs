﻿using Tobe.Core.Mvc.Implementations;
using UnityEngine;

namespace Tobe.Core
{
    [CreateAssetMenu(fileName = nameof(AdditiveSceneLoadGroup),
        menuName = "Scene Manager/" + nameof(AdditiveSceneLoadGroup))]
    public class AdditiveSceneLoadGroup : ScriptableObject
    {
        public string transitionTitle = "Loading ...";

        public string[] transitionTips = new []{ "Please wait, it will be ready soon."};
        
        [Scene]
        public string mainScene;
        
        [Scene]
        public string[] decoratingScenes;
    }
}