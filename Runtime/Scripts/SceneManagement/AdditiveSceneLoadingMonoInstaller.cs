﻿using Zenject;

namespace Tobe.Core
{
    public class AdditiveSceneLoadingMonoInstaller : MonoInstaller<AdditiveSceneLoadingMonoInstaller>
    {
        public AdditiveManagerSettings additiveManagerSettings;
        public override void InstallBindings()
        {
            Container
                .BindInterfacesAndSelfTo<AdditiveManagerSettings>()
                .FromInstance(additiveManagerSettings)
                .AsSingle();

            AdditiveSceneLoadingInstaller.Install(Container);
            
        }
    }
}