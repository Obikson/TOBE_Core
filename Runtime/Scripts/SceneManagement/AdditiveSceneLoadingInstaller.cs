﻿using Zenject;

namespace Tobe.Core
{
    public class AdditiveSceneLoadingInstaller : Installer<AdditiveSceneLoadingInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<AdditiveSceneManager>().AsSingle();
        }
    }
}