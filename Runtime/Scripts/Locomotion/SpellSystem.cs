﻿using System;
using UniRx;
using UnityEngine;

public class SpellSystem
{
    private SpellSheet[] _spells;

    public SpellSystem(CharacterClassSheet characterClassSheet, UnitBodyAnimator bodyAnimator, IUnitIdentifier combatGroup,
        UnitContainerContext containerContext, Projectile.Factory spellProjectileFactory)
    {
        _spells = characterClassSheet.spellSheets;

        bodyAnimator.AbilityUsed.Subscribe(x =>
        {
            MonoBehaviour.print("Used ability " + x);
            foreach (var spellEffect in _spells[0].effects)
            {
                if (spellEffect.projectile)
                {
                    spellProjectileFactory.Create(new ProjectileSpawnArgs
                    {
                        Lifetime = 3,
                        DamageSource = new SpellDamageSource(combatGroup),
                        Position = containerContext.projectileSpawner.position,
                        Rotation = containerContext.projectileSpawner.rotation,
                        ModelPrefab = spellEffect.projectile,
                        SpellEffectId = spellEffect.id
                    });
                }
            }
        });
    }
}