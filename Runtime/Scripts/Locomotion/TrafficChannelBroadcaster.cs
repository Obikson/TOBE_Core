﻿using UniRx;
using Zenject;

public class TrafficChannelBroadcaster<TSignal> where TSignal : class
{
    private readonly SignalBus _signalBus;

    public TrafficChannelBroadcaster(SignalBus signalBus)
    {
        _signalBus = signalBus;
    }

    public void BroadcastGlobalMessage(TSignal val)
    {
        MessageBroker.Default.Publish(val);
    }

    public void FireSignal()
    {
        _signalBus.Fire<TSignal>();
    }

    public void FireSignal(TSignal val)
    {
        _signalBus.Fire(val);
    }

    public void FireSignalId(object id)
    {
        _signalBus.FireId<TSignal>(id);
    }

    public void FireSignalId(object id, TSignal val)
    {
        _signalBus.FireId(id, val);
    }
}