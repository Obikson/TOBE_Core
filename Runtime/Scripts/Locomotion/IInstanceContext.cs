﻿using System;
using UniRx;

public interface IInstanceContext : IDisposable
{
    IReadOnlyReactiveCollection<IAiTarget> Targets { get; }
    void RegisterTarget(IAiTarget item);
    void UnregisterTarget(IAiTarget item);
}