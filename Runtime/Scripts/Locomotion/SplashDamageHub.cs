﻿using System;
using UniRx;
using UnityEngine;
using Zenject;

public class CombatSystemInstaller : Installer<CombatSystemInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<SplashDamageHub>().AsSingle();
    }
}

public class SplashDamageEffect
{
    public IDamageSource DamageSource { get; set; }
    public int Amount { get; set; }
    public Vector3 Origin { get; set; }

    public Quaternion? Direction { get; set; }

    public float? RadiusDeg { get; set; }
    public double Range { get; set; }
}

public interface ISplashDamageBroadcaster
{
    void DealSplashDamage(IDamageSource source, int amount, Vector3 origin, double range);

    void DealSplashDamage(IDamageSource source, int amount, Vector3 origin, double range, Quaternion direction,
        float radiusDeg = 360);
}

public interface ISplashDamageObserver
{
    IObservable<SplashDamageEffect> SplashDamage { get; }
}

public class SplashDamageHub : IDisposable, ISplashDamageBroadcaster, ISplashDamageObserver
{
    public IObservable<SplashDamageEffect> SplashDamage => _splashDamage;

    private readonly Subject<SplashDamageEffect> _splashDamage;

    public SplashDamageHub()
    {
        _splashDamage = new Subject<SplashDamageEffect>();
    }

    public void DealSplashDamage(IDamageSource source, int amount, Vector3 origin, double range)
    {
        if (source == null)
            throw new ArgumentNullException();

        _splashDamage.OnNext(new SplashDamageEffect
        {
            DamageSource = source,
            Amount = amount,
            Origin = origin,
            Range = range
        });
    }

    public void DealSplashDamage(IDamageSource source, int amount, Vector3 origin, double range, Quaternion direction,
        float radiusDeg = 360)
    {
        if (source == null)
            throw new ArgumentNullException();

        _splashDamage.OnNext(new SplashDamageEffect
        {
            DamageSource = source,
            Amount = amount,
            Origin = origin,
            Direction = direction,
            RadiusDeg = radiusDeg, Range = range
        });
    }

    public void Dispose()
    {
        _splashDamage?.Dispose();
    }
}