﻿public class Shield : Weapon
{
    public override bool IsMelee => true;
   

    public override bool CanMelee => false;
    public override bool CanHaveShield => false;

    public override void Use(ItemUsagePhase phase, IUnitIdentifier groupUsingIt = null)
    {
    }
}