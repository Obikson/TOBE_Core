﻿using System;
using UnityEngine;
using Zenject;

public class MountableItemSlot : ItemSlotBase
{
    public static readonly string ItemPrefabParamName = nameof(itemPrefab);

    [SerializeField] protected MountableItem itemPrefab;

    protected MountableItem ItemInstance;

    private DiContainer _diContainer;

    private UnitContainerContext _unitContainerContext;

    [Inject]
    private void Ctor(DiContainer diContainer, UnitContainerContext unitContainerContext)
    {
        _unitContainerContext = unitContainerContext;
        _diContainer = diContainer;
    }

    //Todo: this has to be repository
    public void ChangeItem(MountableItem item)
    {
        if (ItemInstance)
        {
            Destroy(ItemInstance.gameObject);
            ItemInstance = null;
        }

        itemPrefab = item;

        SpawnItem();
    }

    private void Start()
    {
        SpawnItem();
    }

    private void SpawnItem()
    {
        if (ItemInstance || !itemPrefab) return;

        ItemInstance = _diContainer.InstantiatePrefabForComponent<MountableItem>(itemPrefab, transform);

        ItemInstance.transform.localPosition = Vector3.zero;

        ItemInstance.transform.localRotation = Quaternion.identity;

        OnSpawned();
    }

    protected virtual void OnSpawned()
    {
    }

    public MountableItem GetItemInstanceTyped()
    {
        if (!ItemInstance)
            SpawnItem();

        return ItemInstance;
    }

    public override Transform GetItemInstance()
    {
        if (!ItemInstance)
            SpawnItem();

        return ItemInstance.transform;
    }
}