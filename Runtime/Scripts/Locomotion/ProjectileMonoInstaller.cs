﻿using UnityEngine;
using Zenject;


public class ProjectileMonoInstaller : MonoInstaller<ProjectileMonoInstaller>
{
    [SerializeField] private Projectile prefab;

    public ProjectileRepository repository;
    
    public override void InstallBindings()
    {
        Container.Bind<ProjectileRepository>().FromInstance(repository).AsSingle();
        
        Container.BindFactory<ProjectileSpawnArgs, Projectile, Projectile.Factory>()
            .FromMonoPoolableMemoryPool(x =>
                x.WithInitialSize(10).FromComponentInNewPrefab(prefab).UnderTransformGroup("Projectile Pool"));
    }
}