﻿using System;
using UniRx;
using Zenject;

public class TrafficChannelReceiver<TSignal> where TSignal : class
{
    private readonly SignalBus _signalBus;

    public TrafficChannelReceiver(SignalBus signalBus)
    {
        _signalBus = signalBus;
    }

    public IObservable<TSignal> SubscribeToGlobalMessage()
    {
        return MessageBroker.Default.Receive<TSignal>();
    }

    public void SubscribeToSignal(Action callback)
    {
        _signalBus.Subscribe<TSignal>(callback);
    }

    public void SubscribeToSignalId(object id, Action callback)
    {
        _signalBus.SubscribeId<TSignal>(id, callback);
    }

    public void SubscribeToSignal(Action<TSignal> callback)
    {
        _signalBus.Subscribe(callback);
    }


    public void SubscribeToSignalId(object id, Action<TSignal> callback)
    {
        _signalBus.SubscribeId(id, callback);
    }
}