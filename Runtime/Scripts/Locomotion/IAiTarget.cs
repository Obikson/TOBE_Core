﻿using UnityEngine;

public interface IAiTarget : IHittable
{
    Vector3 WorldPosition { get; }
}