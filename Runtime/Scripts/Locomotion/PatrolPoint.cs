﻿using UnityEngine;

public class PatrolPoint : MonoBehaviour
{
    public int Id { get; set; }

    
    
    public double Range
    {
        get => _range;
        set => _range = value;
    }

    public float idleSeconds = 0;
    [SerializeField] private double _range = 3f;
}