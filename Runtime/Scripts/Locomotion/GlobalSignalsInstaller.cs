﻿using Zenject;

public class GlobalSignalsInstaller : Installer<GlobalSignalsInstaller>
{
    public override void InstallBindings()
    {
        Container.DeclareSignal<DialogOpportunitySignal>();
        Container.DeclareSignal<QuestKillProgressSignal>();
    }
}