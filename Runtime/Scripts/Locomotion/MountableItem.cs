﻿using System;
using Sirenix.OdinInspector;
using Tobe.Core;
using UnityEngine;

public interface IUsableItem
{
    void Use(ItemUsagePhase phase, IUnitIdentifier groupUsingIt);
}

public enum ItemUsagePhase
{
    Idle,
    AttackPrepare,
    AttackRelease,
    TriggerItem
}

public abstract class MountableItem : MonoBehaviour, IStashableItem, IDisposable
{
    [SerializeField] protected AnimatorOverrideController animatorOverrideController;

    public StanceSpeedModifiers motionSpeedModifiers;

    [Required] public Transform inSlotTransform;

    [Required] public Transform inHandTransform;

    [Required] public GameObject model;

    [SerializeField] private int id;

    public int Id => id;
    
    public ItemSlotUsage slotUsage = ItemSlotUsage.Unknown;

    public void Stash()
    {
        Dispose();
    }


    public void Mount(Transform primaryHand, Transform secondaryHand)
    {
        transform.SetParent(primaryHand, false);

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        if (model)
        {
            model.transform.SetParent(inHandTransform, false);
        }

        OnMount(primaryHand, secondaryHand);
    }

    protected virtual void OnMount(Transform primaryHand, Transform secondaryHand)
    {
    }

    public void Dismount(Transform itemSlotBase)
    {
        transform.SetParent(itemSlotBase, false);

        if (model)
        {
            if (!inSlotTransform)
            {
                Debug.LogWarning("Weapon " + this + " has no \"In Slot Transform Set\"");
            }

            model.transform.SetParent(inSlotTransform, false);
        }

        transform.localPosition = Vector3.zero;

        transform.localRotation = Quaternion.identity;
    }


    public void Dispose()
    {
        Destroy(gameObject);
    }

    public Vector3 WorldPosition => transform.position;
}