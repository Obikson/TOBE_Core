﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tobe.Core;
using UniRx;
using UnityEngine;
using Zenject;


public class UnitInventorySystem
{
    private DiContainer _diContainer;

    public TItemType GetItemInstance<TItemType>(int id) where TItemType : Component
    {
        var item = _itemRepository.Items[id];

        return _diContainer.InstantiatePrefabForComponent<TItemType>(item.prefab);
    }

    private readonly List<int> _items;

    //Todo: delete later and use for ex.: equip signal
    private ItemSlotRig _itemSlotRig;

    private readonly IItemRepository _itemRepository;


    public UnitInventorySystem(SignalBus signalBus, IItemRepository itemRepository, DiContainer diContainer, ItemSlotRig itemSlotRig)
    {
        signalBus.Subscribe<ItemStashSignal>(x => StashItem(x.StashableItem));
        _itemRepository = itemRepository;
        _items = new List<int>();
        _diContainer = diContainer;
        _itemSlotRig = itemSlotRig;
    }

    public void StashItem(IStashableItem item)
    {
        
        _items.Add(item.Id);
        
        //Todo: only now for debug instant equip
        if (item is MountableItem mountableItem)
        {
            _itemSlotRig.GetSlot(mountableItem.slotUsage)?.ChangeItem(mountableItem);
        }
        
        item.Stash();
        
        MessageBroker.Default.Publish(new ItemStashedGlobalMessage
        {
            StashableItem = item
        });
    }
}