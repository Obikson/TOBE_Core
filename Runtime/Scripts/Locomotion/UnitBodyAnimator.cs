﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using Tobe.Core;
using Tobe.Core.Mvc.Implementations;
using UniRx;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

[Serializable]
public class LocomotionTweak
{
    public float forwardMultiplier = 5f;
    public float backwardMultiplier = 1.2f;
    public float angularSpeed = 70;
}

public class UnitBodyAnimator : MonoBehaviour, IUnitBodyAnimator
{
    [SerializeField] private UnitBodyAnimatorParameters parameters;

    [SerializeField] private UnitBodyAnimatorLayers layers;

    [SerializeField] private UnitBodyAnimatorStates states;

    [SerializeField] private UnitBodyAnimationEvents animationEvents;

    [SerializeField] private bool randomizeAnimationStart;

    public AnimationClip startingAnimation;

    [TabGroup("Basic")] [SerializeField] private MonoAnimator monoAnimator;

    private Subject<CharacterAbilityType> _abilityUsed;

    private bool _isInEquippingAnimation;

    public BoolReactiveProperty HasItemEquipped { get; private set; }


    public IObservable<ItemUsagePhase> AnimatorItemUsagePhase => _animatorItemUsagePhase;

    public IObservable<CharacterAbilityType> AbilityUsed => _abilityUsed;
    public BoolReactiveProperty IsInCombat => _isInCombat;


    private Subject<ItemUsagePhase> _animatorItemUsagePhase;

    private BoolReactiveProperty _isInCombat;


    [Inject]
    private void Ctor(Transform root)
    {
        _abilityUsed = new Subject<CharacterAbilityType>();

        _animatorItemUsagePhase = new Subject<ItemUsagePhase>();

        HasItemEquipped = new BoolReactiveProperty();

        HasItemEquipped.Subscribe(x => monoAnimator.SetParameter(parameters.HasWeaponEquipped, x));

        _isInCombat = new BoolReactiveProperty();

        _isInCombat.Subscribe(x => { monoAnimator.SetLayerWeight(layers.CombatLayerIndex, x ? 1 : 0); });

        Observable.EveryUpdate().Select(x => root.position).Buffer(TimeSpan.FromMilliseconds(200))
            .Where(x => x.Count > 1).Subscribe(x =>
            {
                var last = x.Last();

                var first = x.First();

                var sum = root.InverseTransformDirection(last).z - root.InverseTransformDirection(first).z;

                monoAnimator.SetParameter(parameters.VelocityZHandle, sum);
                monoAnimator.SetParameter(parameters.LocomotionVariant, sum);
            });
    }

    private void Awake()
    {
        monoAnimator.StateExited += (sender, handler) =>
        {
            if (handler.AnimatorStateInfo.IsName(states.DisarmWeapon))
            {
                _isInEquippingAnimation = false;
                HasItemEquipped.Value = false;
            }
            else if (handler.AnimatorStateInfo.IsName(states.EquipWeapon))
            {
                _isInEquippingAnimation = false;
                HasItemEquipped.Value = true;
            }
            else if (handler.AnimatorStateInfo.IsName(states.Wave))
            {
                monoAnimator.SetLayerWeight(layers.SocialsLayerIndex, 0);
            }
        };

        monoAnimator.animationEventFired.AsObservable().Subscribe(x =>
        {
            var valueToLower = x.stringValue.ToLower();
            if (valueToLower.Equals(animationEvents.hitTakenEventValue.ToLower()))
            {
                _animatorItemUsagePhase.OnNext(ItemUsagePhase.TriggerItem);
            }
            else
            {
                var spellSlot =
                    states.SlotMappings.FirstOrDefault(mapping => mapping.stateName.ToLower().Equals(valueToLower));

                if (spellSlot != null)
                    _abilityUsed.OnNext(spellSlot.abilityType);
            }
        });
    }

    private void Start()
    {
        parameters.Initialize();

        layers.Initialize(monoAnimator);

        if (randomizeAnimationStart)
        {
            monoAnimator.SetPlaybackTime(Random.Range(0, startingAnimation.length));
        }
    }


    public void PhysicalAttackRelease()
    {
        if (_isInEquippingAnimation || !_isInCombat.Value) return;

        monoAnimator.SetTrigger(parameters.WeaponAttackPhaseTrigger);
        _animatorItemUsagePhase.OnNext(ItemUsagePhase.AttackRelease);
    }

    public void PhysicalAttackPrepare()
    {
        if (_isInEquippingAnimation) return;

        if (!HasItemEquipped.Value)
        {
            DrawWeapon();
            return;
        }

        monoAnimator.PlayState(states.WeaponAttackPhase1, layers.CombatLayerIndex);
        _animatorItemUsagePhase.OnNext(ItemUsagePhase.AttackPrepare);
    }

    public void Death()
    {
        monoAnimator.SetLayerWeight(layers.CombatLayerIndex, 0);
        monoAnimator.SetLayerWeight(layers.SocialsLayerIndex, 0);
        monoAnimator.PlayState(states.Death, 0);
    }

    public void SpellSlot(int slotNum)
    {
        monoAnimator.PlayState("Spell Slot " + slotNum);
    }

    public void DrawWeapon()
    {
        if (!_isInCombat.Value)
            _isInCombat.Value = true;

        if (_isInEquippingAnimation) return;
        _isInEquippingAnimation = true;

        monoAnimator.SetTrigger(parameters.DrawWeaponTrigger);
    }

    public void DisarmWeapon()
    {
        if (_isInEquippingAnimation) return;
        _isInEquippingAnimation = true;

        if (_isInCombat.Value)
            _isInCombat.Value = false;

        monoAnimator.SetTrigger(parameters.DisarmWeaponTrigger);
    }

    public void Block()
    {
        monoAnimator.PlayState(states.Block, layers.CombatLayerIndex);
    }

    public void TakeHit()
    {
        monoAnimator.PlayState(states.TakeHit, layers.CombatLayerIndex);
    }

    private void UseAnimatorOverride(AnimatorOverrideController overrideController)
    {
        if (monoAnimator.RuntimeAnimator == overrideController)
        {
            return;
        }

        monoAnimator.RuntimeAnimator = overrideController;
    }

    private void ResetOverride()
    {
        monoAnimator.ResetController();
    }

    public void ExecSocials(SocialsData data)
    {
        monoAnimator.SetLayerWeight(layers.SocialsLayerIndex, 1);

        monoAnimator.PlayState(data.clipName);
    }

    public void PlayIdleVariations()
    {
        monoAnimator.PlayState(states.RandomIdleVariation(), 0);
    }


    private Vector3 _lastPosition;

    public void Dispose()
    {
        _abilityUsed?.Dispose();
        _animatorItemUsagePhase?.Dispose();
        _isInCombat?.Dispose();
        HasItemEquipped?.Dispose();
    }
}