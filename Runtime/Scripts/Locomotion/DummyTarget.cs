﻿using System;
using UnityEngine;

public class DummyTarget : MonoBehaviour, IAiTarget
{
    public class DummyUnitIdentifier : IUnitIdentifier
    {
        public DummyUnitIdentifier(string unitId, int combatGroupId)
        {
            UnitId = unitId;
            CombatGroupId = combatGroupId;
        }

        public string UnitId { get; }
        public int CombatGroupId { get; }
    }

    [SerializeField] private int groupId;

    public string unitId;

    public void TakeHit(IDamageSource damageSource, int damageAmount)
    {
        print("Dmg dealt " + damageAmount);
    }

    public IUnitIdentifier UnitIdentifier => new DummyUnitIdentifier(unitId, groupId);

    public Vector3 WorldPosition => transform.position;
}