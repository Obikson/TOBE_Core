﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using Zenject;

public class ItemSlotRig : MonoBehaviour
{
    [SerializeField] public Transform projectileSpawn;

    [SerializeField] private Transform mainHandGripPoint;

    [SerializeField] public Transform otherHandGripPoint;

    public List<MountableItemSlot> weaponSlots;

    private UnitInventorySystem _unitInventorySystem;


    [Inject]
    private void Ctor(UnitInventorySystem inventorySystem)
    {
        _unitInventorySystem = inventorySystem;
        PrimaryHandWeapon = new ReactiveProperty<MountableItem>();
    }

    private void Awake()
    {
        PrimaryHandWeapon.Where(x => x)
            .Subscribe(x => x.Mount(mainHandGripPoint.transform, otherHandGripPoint.transform));

        PrimaryHandWeapon.Where(x => !x)
            .Subscribe(x =>
            {
                var slot = GetSlot(ItemSlotUsage.PrimaryWeapon);
                
                if (!slot) return;
                
                var instance = slot.GetItemInstanceTyped();

                if (instance)
                    instance.Dismount(slot.transform);
            });
    }


    public ReactiveProperty<MountableItem> PrimaryHandWeapon { get; private set; }

    [Button]
    public void EquipItem(int id)
    {
        var tt = _unitInventorySystem.GetItemInstance<MountableItem>(id);

        if (tt)
        {
            var slot = GetSlot(tt.slotUsage);

            if (slot)
            {
                slot.ChangeItem(tt);
            }
        }
    }

    public MountableItemSlot GetSlot(ItemSlotUsage slotUsage)
    {
        return weaponSlots.OfType<MountableItemSlot>().FirstOrDefault(x => x.slotUsage == slotUsage);
    }
}