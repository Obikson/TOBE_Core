﻿using UnityEngine;

public enum ItemSlotUsage
{
    Unknown = 0,
    PrimaryWeapon = 2,
    SecondaryWeapon = 2,
}

public abstract class ItemSlotBase : MonoBehaviour
{
    public abstract Transform GetItemInstance();
    public bool HasItem => GetItemInstance();

    
    
    public ItemSlotUsage slotUsage;
}