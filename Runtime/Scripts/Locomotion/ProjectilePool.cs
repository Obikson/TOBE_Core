﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;


public class ProjectileSpawnArgs
{
    public IDamageSource DamageSource { get; set; }
    
    public Vector3 Position { get; set; }
    public Quaternion Rotation { get; set; }
    public float Lifetime { get; set; }
    public GameObject ModelPrefab { get; set; }
    public string SpellEffectId { get; set; }
}
