﻿using System;
using TMPro;
using Tobe.Core;
using Tobe.Core.Mvc;
using Tobe.Core.Mvc.Implementations;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class ConfirmableWindowView : IView<ConfirmableWindowViewModel>
{
    [SerializeField] private GameObject root;

    [SerializeField] private TobeButton confirmButton;
    [SerializeField] private TobeLabel label;

    public void RenderModel(ConfirmableWindowViewModel model)
    {
        root.SetActive(model.IsVisible);
        
        confirmButton.SetText(model.ButtonText);
        
        label.SetText(model.Label);
        
        confirmButton.Click.RemoveAllListeners();
        confirmButton.Click.AddListener(() => model.OnConfirm?.Invoke());
    }

    public GameObject Root => root;
    public TobeButton ConfirmButton => confirmButton;
}

[Serializable]
public class ConfirmableWindowViewModel : IDirtyModel
{
    public bool IsDirty { get; set; }
    public Action OnConfirm { get; set; }
    public string ButtonText { get; set; } = "OK";
    public string Label { get; set; }
    public bool IsVisible;
}

public interface IUiWindow : IUiElement
{
    void Deactivate();

    void Activate();
}

public interface IConfirmableUiWindowController : IController<ConfirmableWindowView, ConfirmableWindowViewModel>,
    IUiWindow
{
}

public class ConfirmableUiWindowController : Controller<ConfirmableWindowView, ConfirmableWindowViewModel>,
    IConfirmableUiWindowController
{
    public void Deactivate()
    {
        View.Root.SetActive(false);
    }

    public void Activate()
    {
        View.Root.SetActive(true);
    }
}