﻿using Tobe.Core.Mvc;
using Zenject;


public class InteractiveUiMonoInstaller : MonoInstaller<InteractiveUiMonoInstaller>
{
    public ConfirmableUiWindow interactionsPromptWindow;

    public DialogWindow dialogWindow;

    public ConfirmableUiWindow achievementWindow;
    
    public override void InstallBindings()
    {
        Container.BindViewController<ConfirmableWindowView, IConfirmableUiWindowController>()
            .WithControllerImplementation<ConfirmableUiWindowController>();

        Container.BindViewController<DialogWindowView, IDialogWindowController>()
            .WithControllerImplementation<DialogWindowController>();

        Container.Bind<IInteractionUiManager>()
            .FromInstance(new InteractionUiManager(interactionsPromptWindow, dialogWindow, achievementWindow)).AsSingle();

    }
    
    
}

