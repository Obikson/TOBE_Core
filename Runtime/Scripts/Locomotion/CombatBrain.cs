﻿using System;
using System.Collections;
using System.Linq;
using UniRx;
using UnityEngine;


public class CombatBrain : IAiBrain
{
    private readonly ReactiveProperty<ICommand> _currentCommand;

    public IReadOnlyReactiveProperty<ICommand> CurrentCommand => _currentCommand;

    private IDisposable _searchMicrocoroutine;

    private IDisposable _attackCoroutine;

    private IAiTarget _currentTarget;

    public CombatBrain(UnitContainerContext unitContainerContext,
        IInstanceContext instanceContext,
        IUnitIdentifier identifier,
        EnemyContainerContext containerContext,
        UnitBodyAnimator animator, ItemSlotRig itemSlotRig
    )
    {
        _currentCommand = new ReactiveProperty<ICommand>();

        _searchMicrocoroutine = Observable.EveryUpdate().Subscribe(x =>
        {
            if (_currentTarget == null)
            {
                var aiTarget = instanceContext.Targets.FirstOrDefault(target =>
                    target.UnitIdentifier.CombatGroupId != identifier.CombatGroupId &&
                    IsInRange(unitContainerContext.root.position, target.WorldPosition,
                        containerContext.aggressiveRange));

                if (aiTarget != null && _currentTarget == null)
                {
                    _currentTarget = aiTarget;
                    _currentCommand.Value = new KillCommand(aiTarget);
                    animator.IsInCombat.Value = true;
                    animator.DrawWeapon();
                }
            }
            else
            {
                var isInRange = IsInRange(unitContainerContext.root.position, _currentTarget.WorldPosition,
                    containerContext.aggressiveRange);

                if (isInRange)
                {
                    if (itemSlotRig.PrimaryHandWeapon.Value != null &&
                        itemSlotRig.PrimaryHandWeapon.Value is Weapon weapon)
                    {
                        //TODO: implement weapon range
                        if (_attackCoroutine == null)
                        {
                            //ERROR: Not repeating
                            _attackCoroutine = Observable.FromMicroCoroutine(Attack).Subscribe(i =>
                            {
                                MonoBehaviour.print("Attack");
                                animator.PhysicalAttackPrepare();
                                animator.PhysicalAttackRelease();
                            });
                        }
                    }
                }
                else
                {
                    if (_attackCoroutine != null)
                    {
                        MonoBehaviour.print("Attack command disposed");
                        _attackCoroutine?.Dispose();
                        _attackCoroutine = null;
                    }

                    _currentTarget = null;
                    _currentCommand.Value = new PatrolCommand();
                }
            }
        });
    }

    private IEnumerator Attack()
    {
        while (_currentTarget != null)
        {
            yield return new WaitForSeconds(.2f);
        }
    }


    private bool IsInRange(Vector3 myWorldPosition, Vector3 targetWorldPosition, float aggressiveRange)
    {
        return Vector3.Distance(myWorldPosition, targetWorldPosition) < aggressiveRange;
    }

    public bool TryInterupt(ICommand command)
    {
        return false;
    }

    public void Dispose()
    {
        _currentTarget = null;
        _attackCoroutine?.Dispose();
        _searchMicrocoroutine?.Dispose();
        _currentCommand?.Dispose();
    }
}