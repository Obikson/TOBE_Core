﻿public interface IInteractionUiManager
{
    IConfirmableUiWindowController InteractionPromptWindowController { get; }
    IConfirmableUiWindowController AchievementWindow { get; }
    IDialogWindowController DialogWindowController { get; }
}