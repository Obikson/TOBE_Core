﻿using System.Collections.Generic;
using Tobe.Core;
using UniRx;
using Zenject;

public class ItemStashSignal
{
    public IStashableItem StashableItem { get; set; }
}

public interface IUnitInteractionSystem
{
}

public class UnitInteractionSystem : IUnitInteractionSystem
{
}

public class PlayerInteractionSystem : IUnitInteractionSystem
{
    private readonly IInteractionUiManager _interactionUiManager;

    private readonly List<IInteractable> _itemsInteractingNow;

    private readonly SignalBus _signalBus;

    private readonly IUnitIdentifier _localTarget;

    private readonly IItemRepository _itemRepository;
    private IInteractable _localInteractable;
    public PlayerInteractionSystem(ReactiveProximityWatcher proximityWatcher,
        IUnitIdentifier localTarget,
        IInteractionUiManager interactionUiManager,
        SignalBus signalBus,
        IItemRepository itemRepository, IInteractable localInteractable)
    {
        _localInteractable = localInteractable;
        _localTarget = localTarget;
        _interactionUiManager = interactionUiManager;
        _signalBus = signalBus;
        _itemRepository = itemRepository;


        _itemsInteractingNow = new List<IInteractable>();

        proximityWatcher.OnGameObjectEnter.Where(x => !x.IsCollision)
            .SelectMany(x => x.GameObject.GetComponentsInterfaced<IInteractable>())
            .Where(x => x != null)
            .Subscribe(InteractableFound);

        proximityWatcher.OnGameObjectExit.Where(x => !x.IsCollision)
            .SelectMany(x => x.GameObject.GetComponentsInterfaced<IInteractable>())
            .Where(x => x != null)
            .Subscribe(InteractableLost);
    }

    private void InteractableFound(IInteractable obj)
    {
        //Todo: maybe check it?
        _itemsInteractingNow.Add(obj);

        if (obj is IStashableItem stashableItem)
        {
            var item = _itemRepository?.Items[stashableItem.Id];
            if (item == null) return;

            _interactionUiManager.InteractionPromptWindowController.BindViewModel(new ConfirmableWindowViewModel
            {
                IsDirty = true,
                IsVisible = true,
                ButtonText = "Pick",
                Label = item.prefab.name,
                OnConfirm = () =>
                {
                    _signalBus.TryFire(new ItemStashSignal
                    {
                        StashableItem = stashableItem
                    });

                    _interactionUiManager.InteractionPromptWindowController.Deactivate();
                },
            });
            _interactionUiManager.InteractionPromptWindowController.Activate();
        }
        else if (obj is IDialogTarget dialogTarget)
        {
            _interactionUiManager.InteractionPromptWindowController.BindViewModel(new ConfirmableWindowViewModel
            {
                IsDirty = true,
                IsVisible = true,
                Label = dialogTarget.DisplayDataProvider.DisplayName,
                OnConfirm = () =>
                {
                    if (dialogTarget.TryBeginDialog(_localTarget, _localInteractable.WorldPosition))
                    {
                        _signalBus.TryFire(new DialogOpportunitySignal
                        {
                            Initiator = _localTarget,
                            Target = dialogTarget,
                            DialogLines = dialogTarget.DialogProvider.GetDialogLines(_localTarget, obj.WorldPosition)
                        });

                        _interactionUiManager.InteractionPromptWindowController.Deactivate();
                    }
                },
                ButtonText = "Talk"
            });
            _interactionUiManager.InteractionPromptWindowController.Activate();
        }
    }

    public void InteractableLost(IInteractable obj)
    {
        _itemsInteractingNow.Remove(obj);

        if (_itemsInteractingNow.Count > 0) return;

        _interactionUiManager.InteractionPromptWindowController.Deactivate();
    }
}

public class DialogOpportunitySignal
{
    public IUnitIdentifier Initiator { get; set; }
    public IDialogTarget Target { get; set; }
    public DialogLine[] DialogLines { get; set; }
}