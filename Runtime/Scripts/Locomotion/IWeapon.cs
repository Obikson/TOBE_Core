﻿using Tobe.Core;
using UnityEngine;

public interface IWeapon : IUsableItem, IStashableItem
{
    bool IsMelee { get; }
    bool CanMelee { get; }
    bool CanHaveShield { get; }
    float Range { get; }
    
    AnimatorOverrideController AnimatorOverrideController { get; }
}