﻿using System.Collections;
using UnityEngine;

public class SpawnFormation : MonoBehaviour
{
    public Transform[] points;

    public int size => points.Length;
}