﻿using Zenject;

public class MeleeWeapon : Weapon
{
    private ISplashDamageBroadcaster _splashDamageBroadcaster;

    public override bool IsMelee => true;

    public override bool CanMelee => true;

    public bool isInBothHands;

    public override bool CanHaveShield => !isInBothHands;

    public override void Use(ItemUsagePhase phase, IUnitIdentifier @groupUsingIt = null)
    {
        //Todo: consider event
        if (phase != ItemUsagePhase.AttackRelease) return;

        _splashDamageBroadcaster.DealSplashDamage(new MeleeDamageSource(groupUsingIt), 1, transform.position, Range);
    }

    [Inject]
    public void Ctor(ISplashDamageBroadcaster splashDamageBroadcaster)
    {
        _splashDamageBroadcaster = splashDamageBroadcaster;
    }

    public class MeleeDamageSource : IDamageSource
    {
        public MeleeDamageSource(IUnitIdentifier unitIdentifier)
        {
            UnitIdentifier = unitIdentifier;
        }

        public IUnitIdentifier UnitIdentifier { get; }
    }
}