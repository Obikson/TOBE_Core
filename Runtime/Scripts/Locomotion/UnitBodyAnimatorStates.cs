﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class UnitBodyAnimatorStatesAbilityMapping
{
    public CharacterAbilityType abilityType;
    public string stateName;

}

[Serializable]
public class UnitBodyAnimatorStates
{
    private string[] idleVariationsStateName = new[]
    {
        "Idle Variation 1",
        "Idle Variation 2",
        "Idle Variation 3"
    };


    [TabGroup("Animator States")] [SerializeField]
    private string blockStateName = "Block";

    [TabGroup("Animator States")] [SerializeField]
    private string takeHitStateName = "Take Hit";

    [TabGroup("Animator States")] [SerializeField]
    private string meleeSlashStateName = "Melee Slash";

    [TabGroup("Animator States")] [SerializeField]
    private string deathStateName = "Death";

    [SerializeField]
    private string locomotionStateName = "Locomotion";

    [SerializeField] private string weaponAttackPhase1 = "Weapon Attack Phase 1";

    [SerializeField] private string weaponSlash = "Weapon Slash";

    public UnitBodyAnimatorStatesAbilityMapping[] SlotMappings = new[]
    {
        new UnitBodyAnimatorStatesAbilityMapping
        {
            abilityType = CharacterAbilityType.SlotAbility1,
            stateName = "Spell Slot 1"
        },
        new UnitBodyAnimatorStatesAbilityMapping
        {
            abilityType = CharacterAbilityType.SlotAbility2,
            stateName = "Spell Slot 2"
        },
        new UnitBodyAnimatorStatesAbilityMapping
        {
            abilityType = CharacterAbilityType.SlotAbility3,
            stateName = "Spell Slot 3"
        },
        new UnitBodyAnimatorStatesAbilityMapping
        {
            abilityType = CharacterAbilityType.SlotAbility4,
            stateName = "Spell Slot 4"
        }
    };

    public string WeaponAttackPhase1 => weaponAttackPhase1;

    public string Death => deathStateName;
    
    public string DisarmWeapon => "Disarm Weapon";
    public string EquipWeapon => "Equip Weapon";
    public string Block => blockStateName;
    public string TakeHit => takeHitStateName;
    public string Wave => "Wave";

    public string IdleVariation => idleVariationsStateName[0];
    public string Locomotion => locomotionStateName;

    public string RandomIdleVariation()
    {
        var ran = Random.Range(0, idleVariationsStateName.Length);
        return idleVariationsStateName[ran];
    }
}