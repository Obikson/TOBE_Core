﻿using System;
using System.Collections.Generic;
using ModestTree;
using Sirenix.OdinInspector;
using Tobe.Core.Mvc.Implementations;
using UnityEngine;
using Zenject;



[ZenjectAllowDuringValidation]
public class RangedWeapon : Weapon, IRangedWeapon, IValidatable
{
    public string DrawShotTriggerParamName = "Draw Trigger";
    public string ReleaseTriggerParamName = "Release Trigger";

    private int _drawShotTriggerParam;
    private int _releaseShotTriggerParam;

    private bool _canMelee;

    public MonoAnimator monoAnimator;

    private Projectile.Factory _projectileFactory;

    private UnitContainerContext _weaponContext;

    [SerializeField] private ProjectileRepository projectileRepository;

    [SerializeField, ShowIf(nameof(HasProjectileRepository)), ValueDropdown(nameof(GetItems))]
    private GameObject projectilePrefab;

    private IEnumerable<GameObject> GetItems()
    {
        return projectileRepository.items;
    }
    
    private bool HasProjectileRepository => projectileRepository;

    [Inject]
    private void Ctor(Projectile.Factory factory, UnitContainerContext weaponContext = null)
    {
        _projectileFactory = factory;
        _weaponContext = weaponContext;
        _drawShotTriggerParam = Animator.StringToHash(DrawShotTriggerParamName);
        _releaseShotTriggerParam = Animator.StringToHash(ReleaseTriggerParamName);
    }

    public override bool IsMelee { get; }

    public class RangedDamageSource : IDamageSource
    {
        public RangedDamageSource(IUnitIdentifier unitIdentifier)
        {
            UnitIdentifier = unitIdentifier;
        }

        public IUnitIdentifier UnitIdentifier { get; }
    }

    private RangedDamageSource _damageSource;

    public override bool CanMelee => _canMelee;

    public bool IsShotDrawn { get; set; }

    private Transform mainHand;

    private Transform otherHand;

    public override bool CanHaveShield => false;

    private bool _isDrawn;

    public override void Use(ItemUsagePhase phase, IUnitIdentifier groupUsingIt)
    {
        switch (phase)
        {
            case ItemUsagePhase.Idle:
                break;
            case ItemUsagePhase.AttackPrepare:
                if (_isDrawn) return;
                _isDrawn = true;
                monoAnimator.SetTrigger(_drawShotTriggerParam);
                IsShotDrawn = true;
                break;
            case ItemUsagePhase.AttackRelease:
                if (!_isDrawn) return;
                _isDrawn = false;
                monoAnimator.SetTrigger(_releaseShotTriggerParam);
                IsShotDrawn = false;
                Shoot(groupUsingIt);
                break;
            case ItemUsagePhase.TriggerItem:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(phase), phase, null);
        }
    }


    public void Shoot(IUnitIdentifier combatGroup)
    {
        if (_damageSource == null)
        {
            _damageSource = new RangedDamageSource(combatGroup);
        }

        _projectileFactory.Create(new ProjectileSpawnArgs
        {
            Position = _weaponContext.projectileSpawner.position,
            Rotation = _weaponContext.projectileSpawner.rotation,
            DamageSource = new RangedDamageSource(combatGroup),
            Lifetime = 3,
            ModelPrefab = projectilePrefab
        });
    }


    private void Update()
    {
        if (IsShotDrawn)
        {
            var dir = otherHand.position - mainHand.position;
            inHandTransform.rotation = Quaternion.LookRotation(dir);
        }
    }

    protected override void OnMount(Transform primaryHand, Transform secondaryHand)
    {
        mainHand = primaryHand;
        otherHand = secondaryHand;
    }


    public void Validate()
    {
        Assert.That(projectilePrefab, "projectilePrefab");
    }
}