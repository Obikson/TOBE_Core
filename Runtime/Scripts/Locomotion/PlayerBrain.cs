﻿using UniRx;

public enum CharacterAbilityType
{
    None,
    PrimaryAbility,
    SlotAbility1,
    SlotAbility2,
    SlotAbility3,
    SlotAbility4
}

public class PlayerBrain : IBrain
{
    
    public PlayerBrain(IPlayerInputs playerInputs, IInstanceContext instanceContext, UnitBodyAnimator bodyAnimator, StatsSystem statsSystem, IAiTarget aiTarget)
    {
        instanceContext.RegisterTarget(aiTarget);
        statsSystem.CurrentLife.Where(x => x <= 0).AsObservable();

        playerInputs.AbilityStates[CharacterAbilityType.PrimaryAbility].Subscribe(doAttack =>
        {
            if (doAttack)
            {
                bodyAnimator.PhysicalAttackPrepare();
            }
            else
            {
                bodyAnimator.PhysicalAttackRelease();
            }
        });

        
        
        playerInputs.AbilityStates[CharacterAbilityType.SlotAbility1].Where(isDown => isDown)
            .Subscribe(value => bodyAnimator.SpellSlot(1));
        playerInputs.AbilityStates[CharacterAbilityType.SlotAbility2].Where(isDown => isDown)
            .Subscribe(value => bodyAnimator.SpellSlot(2));
        playerInputs.AbilityStates[CharacterAbilityType.SlotAbility3].Where(isDown => isDown)
            .Subscribe(value => bodyAnimator.SpellSlot(3));
        playerInputs.AbilityStates[CharacterAbilityType.SlotAbility4].Where(isDown => isDown)
            .Subscribe(value => bodyAnimator.SpellSlot(4));

        playerInputs.EnterCombat.Subscribe(x => bodyAnimator.IsInCombat.Value = x);
    }

    public void Dispose()
    {
    }
}