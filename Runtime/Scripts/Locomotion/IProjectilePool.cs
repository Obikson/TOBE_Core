﻿using UnityEngine;

public interface IProjectilePool
{
    Projectile Spawn( IDamageSource damageSource, Vector3 position, Quaternion rotation);
}