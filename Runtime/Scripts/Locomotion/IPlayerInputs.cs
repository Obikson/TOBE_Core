﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;

public interface IPlayerInputs : IDisposable
{
    IReadOnlyReactiveProperty<Vector3> Velocity { get; }

    IReadOnlyReactiveProperty<bool> EnterCombat { get; }

    IReadOnlyDictionary<CharacterAbilityType, BoolReactiveProperty> AbilityStates { get; }
}


public class KeyboardPlayerInputs : IPlayerInputs, ITickable
{
    private readonly Vector3ReactiveProperty _reactiveVelocity;


    private readonly BoolReactiveProperty _enterCombat;


    private readonly Dictionary<CharacterAbilityType, BoolReactiveProperty> _abilityStates;

    private Vector3 _velocity;

    public IReadOnlyReactiveProperty<Vector3> Velocity => _reactiveVelocity;
    public IReadOnlyReactiveProperty<bool> EnterCombat => _enterCombat;

    public IReadOnlyDictionary<CharacterAbilityType, BoolReactiveProperty> AbilityStates => _abilityStates;

    public KeyboardPlayerInputs()
    {
        _enterCombat = new BoolReactiveProperty();
        _reactiveVelocity = new Vector3ReactiveProperty();
        _abilityStates = new Dictionary<CharacterAbilityType, BoolReactiveProperty>
        {
            {CharacterAbilityType.PrimaryAbility, new BoolReactiveProperty()},
            {CharacterAbilityType.SlotAbility1, new BoolReactiveProperty()},
            {CharacterAbilityType.SlotAbility2, new BoolReactiveProperty()},
            {CharacterAbilityType.SlotAbility3, new BoolReactiveProperty()},
            {CharacterAbilityType.SlotAbility4, new BoolReactiveProperty()},
        };
    }


    public void Tick()
    {
        _velocity[0] = Input.GetAxis("Horizontal");
        _velocity[2] = Input.GetAxis("Vertical");

        _reactiveVelocity.Value = _velocity;


        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            _enterCombat.Value = !EnterCombat.Value;
        }

        _abilityStates[CharacterAbilityType.PrimaryAbility].Value = Input.GetKey(KeyCode.LeftControl);
        _abilityStates[CharacterAbilityType.SlotAbility1].Value = Input.GetKey(KeyCode.Alpha1);
        _abilityStates[CharacterAbilityType.SlotAbility2].Value = Input.GetKey(KeyCode.Alpha2);
        _abilityStates[CharacterAbilityType.SlotAbility3].Value = Input.GetKey(KeyCode.Alpha3);
        _abilityStates[CharacterAbilityType.SlotAbility4].Value = Input.GetKey(KeyCode.Alpha4);
    }

    public void Dispose()
    {
        _reactiveVelocity?.Dispose();
        _enterCombat?.Dispose();
    }
}