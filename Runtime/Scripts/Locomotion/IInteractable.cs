﻿using UnityEngine;

public interface IInteractable
{
    Vector3 WorldPosition { get; }
}