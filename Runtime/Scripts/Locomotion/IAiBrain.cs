﻿using Tobe.Core;
using UnityEngine;

public interface IAiBrain : IBrain,  IStashManager, ICommander
{

}

public class AiBrainContext
{
    public Vector3 WorldPosition { get; set; }
    public IAiTarget PlayerBodyCommander { get; set; }
}
