﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Combat Brain Setup", menuName = "AI/Brains/Combat Brain Setup")]
public class CombatBrainSetup : BrainSetup
{
    public float alertRange = 1f;
    public float chaseRange = 1.1f;
}