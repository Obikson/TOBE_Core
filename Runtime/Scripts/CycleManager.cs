﻿using System.Collections.Generic;
using System.Linq;
using Zenject;

namespace Tobe.Core
{
    public class CycleManager : ICycleManager
    {
        private bool _cycleStarted;
        
        public void Dispose()
        {
            _cycleHandlers.Clear();
        }

        private List<ICycleHandler> _cycleHandlers;

        public CycleManager()
        {
            _cycleHandlers = new List<ICycleHandler>();
        }

        public void Register(ICycleHandler cycleHandler)
        {
            if (_cycleHandlers.Contains(cycleHandler)) return;

            _cycleHandlers.Add(cycleHandler);
        }

       
        public void UnRegister(ICycleHandler cycleHandler)
        {
            _cycleHandlers.Remove(cycleHandler);
        }

        public void StartCycle()
        {
            if(_cycleStarted) return;
            _cycleStarted = true;
            
            _cycleHandlers = _cycleHandlers.Where(x => x != null).ToList();

            var prioritized = _cycleHandlers
                .OfType<ICycleHandlerPrioritizedStart>()
                .OrderByDescending(x => x.CycleStartPriority)
                .ToArray();

            foreach (var prioritizedStart in prioritized)
            {
                prioritizedStart.OnCycleStarted();
            }

            foreach (var resetable in _cycleHandlers.Except(prioritized))
            {
                resetable.OnCycleStarted();
            }
        }

        public void StartCycle<TContext>(TContext context)
        {
            if(_cycleStarted) return;
            _cycleStarted = true;

            _cycleHandlers = _cycleHandlers.Where(x => x != null).ToList();

            var prioritized = _cycleHandlers
                .OfType<ICycleHandlerPrioritizedStart>()
                .OrderByDescending(x => x.CycleStartPriority)
                .ToArray();

            foreach (var prioritizedStart in prioritized)
            {
                if (prioritizedStart is ICycleHandlerPrioritizedStart<TContext> cycleHandlerPrioritizedStart)
                {
                    cycleHandlerPrioritizedStart.OnCycleStarted(context);
                }
                else
                {
                    prioritizedStart.OnCycleStarted();
                }
            }

            foreach (var cycleHandler in _cycleHandlers.Except(prioritized))
            {
                if (cycleHandler is ICycleHandler<TContext> cycleHandlerContextual)
                {
                    cycleHandlerContextual.OnCycleStarted(context);
                }
                else
                {
                    cycleHandler.OnCycleStarted();
                }
            }
        }
        
        public void EndCycle<TContext>(TContext context)
        {
            if(!_cycleStarted) return;
            _cycleStarted = false;
            
            _cycleHandlers = _cycleHandlers.Where(x => x != null).ToList();

            var prioritized = _cycleHandlers
                .OfType<ICycleHandlerPrioritizedEnd>()
                .OrderByDescending(x => x.CycleEndPriority)
                .ToArray();

            foreach (var prioritizedEnd in prioritized)
            {
                if (prioritizedEnd is ICycleHandlerPrioritizedEnd<TContext> prioritizedEndContext)
                {
                    prioritizedEndContext.OnCycleEnded(context);
                }
                else
                {
                    prioritizedEnd.OnCycleEnded();
                }
            }

            foreach (var prioritizedEnd in _cycleHandlers.Except(prioritized))
            {
                if (prioritizedEnd is ICycleHandler<TContext> cycleHandlerContextual)
                {
                    cycleHandlerContextual.OnCycleEnded(context);
                }
                else
                {
                    prioritizedEnd.OnCycleEnded();
                }
            }
        }

        public void EndCycle()
        {
            if(!_cycleStarted) return;
            _cycleStarted = false;

            _cycleHandlers = _cycleHandlers.Where(x => x != null).ToList();

            var prioritized = _cycleHandlers
                .OfType<ICycleHandlerPrioritizedEnd>()
                .OrderByDescending(x => x.CycleEndPriority)
                .ToArray();

            foreach (var prioritizedStart in prioritized)
            {
                prioritizedStart.OnCycleEnded();
            }

            foreach (var resetable in _cycleHandlers.Except(prioritized))
            {
                resetable.OnCycleEnded();
            }
        }
    }
}