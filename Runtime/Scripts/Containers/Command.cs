﻿using System;

namespace Tobe.Core
{
    [Serializable]
    public abstract class Command
    {
        public float Lifetime { get; set; }

        public bool IsComplete { get; set; }

        public Action Completed { get; set; }
    }
}