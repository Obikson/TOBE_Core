﻿namespace Tobe.Core
{
    public interface ICycleHandlerPrioritizedEnd<in TContext> : ICycleHandler<TContext>, ICycleHandlerPrioritizedEnd
    {
    }
    
    
    public interface ICycleHandlerPrioritizedEnd : ICycleHandler
    {
        /// <summary>
        /// Priority in descending order.
        /// </summary>
        int CycleEndPriority { get; }
    }
}