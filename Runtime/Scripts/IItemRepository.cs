﻿using System.Collections.Generic;

namespace Tobe.Core
{
    public interface IItemRepository 
    {
        IReadOnlyDictionary<int, RepositoryItem> Items { get; }
    }
}