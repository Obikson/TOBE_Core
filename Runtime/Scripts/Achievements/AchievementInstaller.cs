﻿using Zenject;

public class AchievementInstaller : Installer<AchievementInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<AchievementService>().AsSingle();
    }
}