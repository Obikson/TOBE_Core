﻿using System;
using System.Collections.Generic;
using System.Linq;
using ModestTree;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "Achievement Repository", menuName = "Repositories/Achievements")]
public class AchievementRepository : ScriptableObject, IAchievementRepository, IValidatable
{
    [SerializeField, ListDrawerSettings(Expanded = true)]
    private Achievement[] achievements;

    public IEnumerable<Achievement> Achievements => achievements;

    public void Validate()
    {
        if (achievements != null && achievements.Any())
            Assert.That(achievements.GroupBy(x => x.id).All(x => x.Count() == 1),
                "achievements.GroupBy(x => x.id).All(x => x.Count() == 1)");
    }
}