﻿using Zenject;

public class AchievementMonoInstaller : MonoInstaller<AchievementMonoInstaller>
{
    public AchievementRepository AchievementRepository;
    
    public override void InstallBindings()
    {
        AchievementInstaller.Install(Container);
        Container.BindInterfacesAndSelfTo<AchievementRepository>().FromInstance(AchievementRepository).AsSingle();
    }
}