﻿using Sirenix.OdinInspector;
using Tobe.Core.Mvc.Implementations;
using UnityEngine;
using Zenject;
using UniRx;

public class AchievementPopup : MonoBehaviour
{
    private string AppearStateName = "Achievement Popup";
    private string DisappearStateName = "Achievement Disappear";
    private int _appearStateId;
    private int _disappearStateId;


    [SerializeField] private MonoAnimator monoBehaviour;

    [SerializeField] private TobeLabel label;

    private IAchievementService _achievementService;

    [Inject]
    private void Ctor(IAchievementService achievementService)
    {
        _achievementService = achievementService;
        _achievementService.AchievementGained.Subscribe(a =>
        {
            label.SetText(a.text);
            Show();
            Invoke(nameof(Hide), 3);
        });
    }

    private void Start()
    {
        _appearStateId = Animator.StringToHash(AppearStateName);
        _disappearStateId = Animator.StringToHash(DisappearStateName);
        gameObject.SetActive(false);
        
    }

    [Button]
    public void Hide()
    {
        monoBehaviour.PlayState(_disappearStateId, 0);

        Invoke(nameof(Disable), 2);
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }

    [Button]
    public void Show()
    {

        gameObject.SetActive(true);
        monoBehaviour.PlayState(_appearStateId, 0);
    }
}