﻿using UniRx;
using UnityEngine;
using Zenject;

public class QuestKillProgressSignal
{
    public int Amount { get; set; }
    public string TargetId { get; set; }
}
