﻿using System;
using UniRx;

public interface IAchievementService
{
    IObservable<Achievement> AchievementGained { get; }

    void GiveAchievement(int id);

    void GiveAchievement(Achievement achievement);
}