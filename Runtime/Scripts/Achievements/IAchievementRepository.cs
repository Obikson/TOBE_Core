﻿using System.Collections.Generic;

public interface IAchievementRepository
{
    IEnumerable<Achievement> Achievements { get; }
}