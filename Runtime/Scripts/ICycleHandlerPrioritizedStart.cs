﻿namespace Tobe.Core
{
    public interface ICycleHandlerPrioritizedStart<TContext> : ICycleHandler<TContext>,ICycleHandlerPrioritizedStart
    {
    }
    
    public interface ICycleHandlerPrioritizedStart : ICycleHandler
    {
        /// <summary>
        /// Priority in descending order.
        /// </summary>
        int CycleStartPriority { get; }
    }
}